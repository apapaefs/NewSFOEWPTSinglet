import Higgs.predictions as HP
import Higgs.bounds as HB
import Higgs.signals as HS
import numpy as np
import pandas as pd
import math

pred = HP.Predictions() # create the model predictions

bounds = HB.Bounds('higgstools/hbdataset') # load HB dataset
signals = HS.Signals('higgstools/hsdataset') # load HS dataset

# add a SM-like Higgs boson with SM-like couplings

h = pred.addParticle(HP.NeutralScalar("h","even"))

# add second BSM Higgs boson which decays to two h bosons and is produced via gluon fusion

H = pred.addParticle(HP.NeutralScalar("H","even"))

# SM Higgs mass
Mh = 125.09
# set the SM Higgs mass
h.setMass(Mh)

# get the SM chi-squared for HiggsSignals
HP.effectiveCouplingInput(h, HP.scaledSMlikeEffCouplings(1.0))
ress_SM = signals(pred)
#print("HiggsSignals chi-sq. for SM=", ress_SM)

# FUNCTION THAT RETURNS HiggsBounds True/False and chi-squared from HiggsSignals
# INPUT IS MH, sintheta and l112, the Scalar-Higgs-Higgs coupling
def check_singlet_point(MH, sintheta, costheta, l112, debug=False):
    # set the couplings of the SM-like Higgs boson to be rescaled according to costheta
    HP.effectiveCouplingInput(h, HP.scaledSMlikeEffCouplings(costheta))
    
    # set the mass of the heavy scalar and rescale the couplings according to sintheta (for production)
    # then set the BRs according to the calculation
    H.setMass(MH)
    HP.effectiveCouplingInput(H, HP.scaledSMlikeEffCouplings(sintheta))
    # calculate and print the heavy H branching ratios, given MH, lambda_112 and sintheta
    heavyBRs = calculate_heavy_BRs_only(BR_interpolators_SM, MH, Mh, l112, sintheta)
    heavyBRs = fix_heavy_BRs(heavyBRs)
    if debug is True:
        print_heavy_Higgs_info(heavyBRs, BR_text_array_heavy_withtripleHiggs, 'Heavy Higgs BRs & width')
    # RESET BRs BEFORE SETTING THEM TO AVOID ISSUES WITH BR>1
    H.setBr('bb', 0.)
    H.setBr('tautau', 0.)
    H.setBr('mumu', 0.)
    H.setBr('cc', 0.)
    H.setBr('ss', 0.)
    H.setBr('tt', 0.)
    H.setBr('gg', 0.)
    H.setBr('gamgam', 0.)
    H.setBr('Zgam', 0.)
    H.setBr('WW', 0.)
    H.setBr('ZZ', 0.)
    # SET THE BRS
    H.setBr('bb', heavyBRs[0])
    H.setBr('tautau', heavyBRs[1])
    H.setBr('mumu', heavyBRs[2])
    H.setBr('cc', heavyBRs[3])
    H.setBr('ss', heavyBRs[4])
    H.setBr('tt', heavyBRs[5])
    H.setBr('gg', heavyBRs[6])
    H.setBr('gamgam', heavyBRs[7])
    H.setBr('Zgam', heavyBRs[8])
    H.setBr('WW', heavyBRs[9])
    H.setBr('ZZ', heavyBRs[10])
    H.setBr('h', 'h', heavyBRs[11])
    H.setTotalWidth(heavyBRs[13])


    # SOME TESTS HERE:
    # test whether the BRs have been set correctly
    if debug is True:
        test_BR_array = [H.br('bb'), H.br('tautau'), H.br('mumu'), H.br('cc'), H.br('ss'), H.br('tt'), H.br('gg'), H.br('gamgam'), H.br('Zgam'), H.br('WW'), H.br('ZZ'), H.br('h', 'h'), 0., H.totalWidth()]
        print_heavy_Higgs_info(test_BR_array, BR_text_array_heavy_withtripleHiggs, 'Heavy Higgs BRs & width TEST')
        print('gg > h cross section @ pp @ 13 TeV=', h.cxn('LHC13', "ggH"))
        print('gg > H cross section @ pp @ 13 TeV=', H.cxn('LHC13', "ggH"))
        # compare to independent calculations:
        xs13_nnlonnll = round_sig(sintheta**2 * XS_interpolator_SM_13TeV_NNLONNLL(MH),5)
        print('independent calculation of the cross section:')
        print('gg > H cross section @ pp @ 13 TeV (N^2LO+NNLL)=',  xs13_nnlonnll)
        

    # get and print the HiggsBounds results
    resb = bounds(pred)
    #print(resb)
    #print(resb.allowed)
    #print(resb.appliedLimits)
    #print([a for a in resb.appliedLimits if "H" in a.contributingParticles()])
    
    # get and print the HiggsSignal result
    ress = signals(pred)
    #print(signals(pred).appliedLimits)
    #print(ress)

    # return HiggsBounds (True/False) for Allowed/Disallowed and the chi-squared from HiggsSignals
    return resb.allowed, ress
