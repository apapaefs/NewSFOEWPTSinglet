#! /usr/bin/env python
import cmath, string, os, sys, fileinput, pprint, math
from math import log10, floor
import numpy as np
import scipy
import operator
import collections
from scipy.interpolate import interp1d
from scipy.interpolate import RegularGridInterpolator
from prettytable import PrettyTable
from numpy import linalg as LA
import Higgs.predictions as HP
import Higgs.bounds as HB
import Higgs.signals as HS
import numpy as np
import pandas as pd
from datetime import date
import matplotlib
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from numpy import vectorize
from scipy.interpolate import splev, splrep

#######################################################
# SETTINGS AND PARAMETERS                             #
#######################################################

# the SM value of the self-coupling and Higgs boson mass in [GeV]
v0 = 246.
MH = 125.0
mt = 172.44 # PDG as of 2019
Mw = 80.385
Mz = 91.1876
Mt = 172.44
Gf = 1.1663787E-5;
v0 = math.sqrt(1/math.sqrt(2.)*(1/Gf));
yt = mt/v0 * math.sqrt(2.);
g0sq = 4. * math.sqrt(2.) * Gf * Mw**2;
g1sq = g0sq * (Mz**2 - Mw**2)/Mw**2
g1 = math.sqrt(g1sq)
g2 = math.sqrt(g0sq)
alpha = 7.2973525693E-3

# get today's date
today = date.today()
todaysdate = today.strftime("%d%m%Y")

#######################################################
# FUNCTIONS TO BE CALLED DURING THE SCAN              #
#######################################################

# import functuons related to conversion of parameter space points
# and calculation of one loop masses:
from singlet_mass_eigenvalues import OneLoop_masses_diag
from singlet_mass_eigenvalues import OneLoop_masses_diag_scale
from singlet_mass_eigenvalues import OneLoop_masses_diag_scale_SARAH
from singlet_mass_eigenvalues import param_SARAH_to_MRM
from singlet_mass_eigenvalues import param_MRM_to_SARAH
# impor functions for constraints from plots:
from singlet_constraint_from_plots import *

# round to a certain number of significance figures 
def round_sig(x, sig=2):
    if x == 0.:
        return 0.
    if math.isnan(x) is True:
        print('Warning, NaN!')
        return 0.
    return round(x, sig-int(floor(log10(abs(x))))-1)

ccount = 0
def next_color():
    global ccount
    color_chosen = colors[ccount]
    if ccount < 8:
        ccount = ccount + 1
    else:
        ccount = 0    
    return color_chosen

# do not increment colour in this case:
def same_color():
    global ccount
    color_chosen = colors[ccount-1]
    return color_chosen

# reset the colour counter
def reset_color():
    global ccount
    ccount = 0

# change the colour scheme   
def change_color_scheme():
    global colors
    colors = ['violet', 'blue', 'green', 'orange', 'red', 'black', 'magenta', 'brown', 'cyan']

# change the colour scheme   
def change_color_scheme2():
    global colors
    colors = ['brown', 'cyan', 'green', 'orange', 'red', 'black', 'magenta', 'brown', 'cyan']

# choose the next colour -- for plotting
cmcount = 0
def next_cmap():
    global cmcount
    cmaps = ['Greens', 'Blues', 'Reds', 'Greys', 'Purples', 'PuRd', 'OrRd', 'PuBu', 'PuBuGn'] # 9 colour maps
    cmap_chosen = cmaps[cmcount]
    if cmcount < 8:
        cmcount = cmcount + 1
    else:
        cmcount = 0    
    return cmap_chosen

# do not increment colour in this case:
def same_cmap():
    global cmcount
    cmaps = ['Greens', 'Blues', 'Reds', 'Greys', 'Purples', 'PuRd', 'OrRd', 'PuBu', 'PuBuGn'] # 9 colour maps
    cmap_chosen = cmaps[cmcount-1]
    return cmap_chosen

def reset_cmap():
    global cmcount
    cmcount = 0


# read the benchmarks processed by PhaseTracer
def ReadProcessedBenchmarks(BenchmarkFile, Tag, MaxPoints):
    BenchmarkFileStream = open(BenchmarkFile, "r")
    BenchmarkDictionary = {}
    counter = 0
    for line in BenchmarkFileStream:
        #print line
        point = [float(line.rstrip().split(',')[12]), float(line.rstrip().split(',')[13])] + [float(x) for x in line.rstrip().split(',')[:7]] + [float(line.rstrip().split(',')[14])] + [float(line.rstrip().split(',')[15])] + [float(line.rstrip().split(',')[16])] + [float(line.rstrip().split(',')[17])] 
        #print Tag + str(counter), point
        BenchmarkDictionary[Tag + str(counter)] = point
        counter = counter + 1
        if counter == MaxPoints: # break at maximum points
            BenchmarkFileStream.close()
            return BenchmarkDictionary
    BenchmarkFileStream.close()
    print('\tRead benchmark file', BenchmarkFile, 'with tag:', Tag)
    return BenchmarkDictionary

# add to the array the benchmarks processed by PhaseTracer
def AddProcessedBenchmarks(BenchmarkDictionary, BenchmarkFile, Tag, MaxPoints):
    BenchmarkFileStream = open(BenchmarkFile, "r")
    counter = int(len(BenchmarkDictionary))
    initial_counter = int(len(BenchmarkDictionary))
    for line in BenchmarkFileStream:
        #print line
        point = [float(line.rstrip().split(',')[12]), float(line.rstrip().split(',')[13])] + [float(x) for x in line.rstrip().split(',')[:7]] + [float(line.rstrip().split(',')[14])] + [float(line.rstrip().split(',')[15])] + [float(line.rstrip().split(',')[16])] + [float(line.rstrip().split(',')[17])] 
        #print Tag + str(counter), point
        BenchmarkDictionary[Tag + str(counter)] = point
        counter = counter + 1
        reduced_counter = counter-initial_counter
        if reduced_counter == MaxPoints: # break at maximum points
            BenchmarkFileStream.close()
            return BenchmarkDictionary
    BenchmarkFileStream.close()
    print('\tRead benchmark file', BenchmarkFile, 'with tag:', Tag)
    return BenchmarkDictionary


# read the benchmarks processed by PhaseTracer -- PERTURBATIVE STABILITY
def ReadProcessedBenchmarksPerturbativityStability(BenchmarkFile, Tag, MaxPoints):
    BenchmarkFileStream = open(BenchmarkFile, "r")
    BenchmarkDictionary = {}
    counter = 0
    for line in BenchmarkFileStream:
        #print line
        point = [float(line.rstrip().split()[0]), float(line.rstrip().split()[1]), float(line.rstrip().split()[2])]  
        #print Tag + str(counter), point
        BenchmarkDictionary[Tag + str(counter)] = point
        counter = counter + 1
        if counter == MaxPoints: # break at maximum points
            BenchmarkFileStream.close()
            return BenchmarkDictionary
    BenchmarkFileStream.close()
    print('\tRead perturbative stability file', BenchmarkFile, 'with tag:', Tag)
    return BenchmarkDictionary

# add to the array the benchmarks processed by PhaseTracer -- PERTURBATIVE STABILITY
def AddProcessedBenchmarksPerturbativityStability(BenchmarkDictionary, BenchmarkFile, Tag, MaxPoints):
    BenchmarkFileStream = open(BenchmarkFile, "r")
    counter = int(len(BenchmarkDictionary))
    initial_counter = int(len(BenchmarkDictionary))
    for line in BenchmarkFileStream:
        #print line
        point = [float(line.rstrip().split()[0]), float(line.rstrip().split()[1]), float(line.rstrip().split()[2])]  
        #print Tag + str(counter), point
        BenchmarkDictionary[Tag + str(counter)] = point
        counter = counter + 1
        reduced_counter = counter-initial_counter
        if reduced_counter == MaxPoints: # break at maximum points
            BenchmarkFileStream.close()
            return BenchmarkDictionary
    BenchmarkFileStream.close()
    print('\tRead perturbative stability file', BenchmarkFile, 'with tag:', Tag)
    return BenchmarkDictionary


# convert benchmarks to the right format
# (i.e. from "SARAH" format to "MRM" format)
def ConvertBenchmarks(OurBenchmark):

    #{246, xsol, Re[mu2sol], Re[MStrial], Re[K1trial], Re[K2trial], Re[\[Kappa]trial], Re[LambdaStrial], Re[\[Lambda]sol], Re[sin\[Theta]]}]
    v0_SARAH = OurBenchmark[0]
    x0_SARAH = OurBenchmark[1]
    mu2_SARAH = OurBenchmark[2]
    MS_SARAH = OurBenchmark[3]
    K1_SARAH = OurBenchmark[4]
    K2_SARAH = OurBenchmark[5]
    Kappa_SARAH = OurBenchmark[6]
    LambdaS_SARAH = OurBenchmark[7]
    Lambda_SARAH = OurBenchmark[8]
    sintheta = OurBenchmark[9]
    #print('sintheta =', sintheta)
    if abs(sintheta) > 1:
        print('sintheta=', sintheta, 'SETTING TO 1')
        sintheta = 1
    costheta = math.sqrt(1 - sintheta**2)
    

    #lam, v0, x0, a1, a2, b3, b4
    
    lamConv, v0Conv, x0Conv, a1Conv, a2Conv, b2Conv, b3Conv, b4Conv = param_SARAH_to_MRM(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH)

    # OneLoop_masses_diag_scale(Sc, lam, v0, x0, a1, a2, b3, b4, g1, g2, yt)
    mh1_1loop, mh2_1loop = OneLoop_masses_diag_scale_SARAH(91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    
    #[ costh, sinth, mass2, gam2,  x0  , lamb, a_1, , a_2  , b_3   , b_4]
    ConvertedBenchmark = [costheta, sintheta, mh2_1loop, -1.0, x0Conv, lamConv, a1Conv, a2Conv, b3Conv, b4Conv]
    return ConvertedBenchmark


# get the SARAH parameters from the benchmark array
def GetSARAHParams(OurBenchmark):
    v0_SARAH = OurBenchmark[0]
    x0_SARAH = OurBenchmark[1]
    mu2_SARAH = OurBenchmark[2]
    MS_SARAH = OurBenchmark[3]
    K1_SARAH = OurBenchmark[4]
    K2_SARAH = OurBenchmark[5]
    Kappa_SARAH = OurBenchmark[6]
    LambdaS_SARAH = OurBenchmark[7]
    Lambda_SARAH = OurBenchmark[8]
    sintheta = OurBenchmark[9]
    return v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta


# calculation of singlet scalar couplings and masses (TREE-LEVEL):
def lambda112(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda112 = v0 * ( a2 - 3 * lam ) * ct**2 * st - 0.5 * a2 * v0 * st**3 + 0.5 * ct * st**2 * ( -a1 - 2 * a2 * x0 + 2 * b3 + 6 * b4 * x0 ) + 0.25 * (a1 + 2 * a2 * x0 ) * ct**3
    return round_sig(lambda112,5)

def lambda122(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda122 =  v0 * ( 3 * lam - a2 ) * st**2 * ct + 0.5 * a2 * v0 * ct**3 + (b3 + 3 * b4 * x0 - 0.5 * a1 - a2 * x0 ) * st * ct**2 + 0.25 * ( a1 + 2 * a2 * x0 ) * st**3 
    return round_sig(lambda122,5)

def lambda111(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda111 = lam * v0 * ct**3 + (0.25 * a1 + 0.5 * a2 * x0) * ct**2 * st + 0.5 * a2 * v0 * st**2 * ct + (b3/3 + b4 * x0) * st**3
    return round_sig(lambda111,5)

def lambda222(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda222 = (4 * (b3 + 3 *b4 *x0) * ct**3 - 6 * a2 * v0 *ct**2 * st + 3 *(a1 + 2 *a2 *x0)* ct* st**2 - 12 * lam * v0 * st**3)/12.
    return round_sig(lambda222,5)

def lambda1111(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda1111 = (lam* ct**4 + a2 * ct**2 * st**2 + b4 * st**4)/4.
    return round_sig(lambda1111,5)

def lambda1112(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda1112 = -(1./4.) * (-b4+lam+(-a2+b4+lam) * (2 * ct**2 - 1) ) *2*ct*st
    return round_sig(lambda1112,5)

def lambda1122(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda1122 = (1./16.)* (a2+3*(b4+lam)+3*(a2-b4-lam) * ((ct**2-st**2)**2 - (st*ct)**2))
    return round_sig(lambda1122,5)

def lambda1222(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda1222 = (1./4.) * (b4-lam+(-a2+b4+lam) * (ct**2 - st**2)) * st*ct
    return round_sig(lambda1222,5)

def lambda2222(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda2222= (1./4.)*(b4 *ct**4 + a2 * ct**2 * st**2 + lam*st**4)
    return round_sig(lambda2222,5)

def calculate_lambdas(lam, a1, a2, x0, v0, costh, sinth, b3, b4):
    l111 = lambda111(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    l112 = lambda112(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    l122 = lambda122(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    l222 = lambda222(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    l1111 = lambda1111(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    l1112 = lambda1112(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    l1122 = lambda1122(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    l1222 = lambda1222(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    l2222 = lambda2222(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    return l111, l112, l122, l222, l1111, l1112, l1122, l1222, l2222

def mh_sq(lam, v0):
    return round_sig(2 * lam * v0**2,10)

def ms_sq(a1, b3, b4, x0, v0):
    return round_sig(b3 * x0 + 2 * b4 * x0**2 - a1 * v0**2 / 4. / x0,10)

def mhs_sq(a1, a2, x0, v0):
    return round_sig((a1 + 2 * a2 * x0 ) * v0/2.,10)

# tree-level masses and mixing angles
def mass_and_mixing(lam, v0, x0, a1, a2, b3, b4):
    # diagonalise mass matrix:
    MASS = np.array( [[mh_sq(lam, v0), mhs_sq(a1, a2, x0, v0)], [mhs_sq(a1, a2, x0, v0), ms_sq(a1, b3, b4, x0, v0)]] )
    w, v = LA.eig(MASS)
    mh2 = cmath.sqrt(w[0]).real
    mh1 = cmath.sqrt(w[1]).real
    if mh1 > mh2:
        mh2_pr = mh2
        mh2 = mh1
        mh1 = mh2_pr
    #print "mh2 = ", mh2, " mh1=", mh1
    sin2theta = (a1 + 2 * a2 * x0) * v0/(w[0] - w[1])
    cos2theta = math.sqrt(1. - sin2theta**2)
    costheta = math.sqrt( (1. + cos2theta)/2.)
    sintheta = math.sqrt(1. - costheta**2)
    return round_sig(mh1,5), round_sig(mh2,5), round_sig(sintheta,5), round_sig(costheta,5)


# print the current parameter point info:
def print_SARAH_params_and_masses(v0, x0, mu2, MS, K1, K2, Kappa, LambdaS, Lambda, sintheta, mh1_1loop, mh2_1loop):
    print('\ntest point params (SARAH):')
    param_names = ['v0', 'x0', 'mu2', 'MS', 'K1', 'K2', 'Kappa', 'LambdaS', 'Lambda', 'sintheta', 'mh1(1loop)', 'mh2(1loop)' ]
    tbl = PrettyTable(["param", "value"])
    tbl.add_row([param_names[0], v0])
    tbl.add_row([param_names[1], x0])
    tbl.add_row([param_names[2], mu2])
    tbl.add_row([param_names[3], MS])
    tbl.add_row([param_names[4], K1])
    tbl.add_row([param_names[5], K2])
    tbl.add_row([param_names[6], Kappa])
    tbl.add_row([param_names[7], LambdaS])
    tbl.add_row([param_names[8], Lambda])
    tbl.add_row([param_names[9], sintheta])
    tbl.add_row([param_names[10], mh1_1loop])
    tbl.add_row([param_names[11], mh2_1loop])

    print(tbl)


# print the current parameter point info:
def print_xsm_params(lam, v0, x0, a1, a2, b3, b4):
    print('\ntest point params (xSM):')
    param_names = ['lam', 'v0', 'x0', 'a1', 'a2', 'b3', 'b4' ]
    tbl = PrettyTable(["param", "value"])
    tbl.add_row([param_names[0], lam])
    tbl.add_row([param_names[1], v0])
    tbl.add_row([param_names[2], x0])
    tbl.add_row([param_names[3], a1])
    tbl.add_row([param_names[4], a2])
    tbl.add_row([param_names[5], b3])
    tbl.add_row([param_names[6], b4])
    
    print(tbl)


def print_scalarcouplings(l111, l112, l122, l222, l1111, l1112, l1122, l1222, l2222):
    print('\nscalar couplings:')
    param_names = ['l111', 'l112', 'l122', 'l222', 'l1111', 'l1112', 'l1122', 'l1222', 'l2222' ]
    
    tbl = PrettyTable(["coupl.", "value"])
    tbl.add_row([param_names[0], l111])
    tbl.add_row([param_names[1], l112])
    tbl.add_row([param_names[2], l122])
    tbl.add_row([param_names[3], l222],divider=True)
    tbl.add_row([param_names[4], l1111])
    tbl.add_row([param_names[5], l1112])
    tbl.add_row([param_names[6], l1122])
    tbl.add_row([param_names[7], l1222])
    tbl.add_row([param_names[8], l2222])

    print(tbl)

def print_htools(hb, hs):
    print('\nHiggsTools results (2σ):')
    param_names = ['HiggsBounds', 'HiggsSignals']
    tbl = PrettyTable(["Test", "result"])
    tbl.add_row([param_names[0], hb])
    tbl.add_row([param_names[1], hs])
    print(tbl)

def print_ewpo(ewpocur, ewpofut):
    print('\nEWPO results (2σ):')
    param_names = ['Current', 'Future']
    tbl = PrettyTable(["Test", "result"])
    tbl.add_row([param_names[0], ewpocur])
    tbl.add_row([param_names[1], ewpofut])
    print(tbl)


def print_wmass(wmass):
    print('\nW mass constraint:')
    param_names = ['Current']
    tbl = PrettyTable(["Test", "result"])
    tbl.add_row([param_names[0],wmass])
    print(tbl)

    
def print_clic(clic14hh, clic3vv, clic3hh):
    print('\nCLIC results (2σ):')
    param_names = ['CLIC h2->h1h1 (1.4 TeV)', 'CLIC h2->VV (3 TeV)', 'CLIC h2->h1h1 (3 TeV)']
    tbl = PrettyTable(["Test", "result"])
    tbl.add_row([param_names[0], clic14hh], divider=True)
    tbl.add_row([param_names[1], clic3vv])
    tbl.add_row([param_names[2], clic3hh])
    print(tbl)

def print_fcc(fcc_hh, fcc_ww, fcc_zz, couplstr_fut):
    print('\nFCC-hh@100 TeV@20/ab results (2σ):')
    param_names = ['sinth < 0.1', 'h2->h1h1', 'h2->WW', 'h2->ZZ']
    tbl = PrettyTable(["Test", "result"])
    tbl.add_row([param_names[0], couplstr_fut])
    tbl.add_row([param_names[1], fcc_hh])
    tbl.add_row([param_names[2], fcc_ww])
    tbl.add_row([param_names[3], fcc_zz])
    print(tbl)
   
############################################################################
# SETUP THE INTERPOLATORS FOR THE BRANCHING RATIOS/WIDTHS                  #
############################################################################

# function to read in the branching ratios into a dictionary in the format:
# mass [GeV] | H -> bbbar | H -> tautau | H -> mumu | H -> cc | H -> ss | H -> tt | H -> gg | H -> gammagamma | H -> Zgamma | H -> WW | H -> ZZ | total width [GeV]
# see https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageBR2014#SM_Higgs_Branching_Ratios_and_Pa
def read_higgsBR(brfile):
    higgsbrs = {}
    brstream = open(brfile, 'r')
    brarray = []
    for line in brstream:
        brarray = [ float(line.split()[1]), float(line.split()[2]), float(line.split()[3]), float(line.split()[4]), float(line.split()[5]), float(line.split()[6]), float(line.split()[7]), float(line.split()[8]), float(line.split()[9]), float(line.split()[10]), float(line.split()[11]), float(line.split()[12])]
        higgsbrs[float(line.split()[0])] = brarray
    # sort by increasing value of HYmass
    sorted_x = sorted(list(higgsbrs.items()), key=operator.itemgetter(0))
    sorted_higgsbrs = collections.OrderedDict(sorted_x)
    return sorted_higgsbrs


# create interpolators for the various BRs and total width and return a dictionary
def interpolate_HiggsBR(brdict):
  # the kind of interpolation
  interpkind = 'cubic'

  # define an array of interpolators
  interp_higgsbrs = []

  # find out how many BRs+width we have:
  values_view = list(brdict.values())
  value_iterator = iter(values_view)
  first_value = next(value_iterator)
  NBRs = len(first_value)
  
  # push back all the values of the masses, brs and width into arrays
  mass_array = []
  br_array =[[] for yy in range(NBRs)]

  # get the mass and the corresponding BR arrays
  for key in list(brdict.keys()):
      mass_array.append(key)
      for ii in range(NBRs):
        br_array[ii].append(brdict[key][ii])

  # now create the interpolators and put them in the array:
  for ii in range(NBRs):
        interpolator = interp1d(mass_array, br_array[ii], kind=interpkind, bounds_error=False)
        interp_higgsbrs.append(interpolator)
  return interp_higgsbrs

# calculate the width h2 -> h1 h1, given the mass, the self-coupling l112 (in GeV) and the sin(mixing angle)
def Gam_h2_to_h1h1(m1, m2, l112, sth):
  if m2 < 2*m1:
    return 0.
  width_h2h1h1 = l112**2 * math.sqrt( 1 - 4 * m1**2 / m2**2 ) / 8 / math.pi / m2
  return width_h2h1h1

# total width of the h2:
def width_h2(sth, m1, m2, l112, Gam_SM):
    total_width = Gam_SM * sth**2  + Gam_h2_to_h1h1(m1, m2, l112, sth)
    return total_width

# the BR h2 -> h1 h1, given the m2, sintheta, l112, Gam_SM (total SM BR)
def BR_h2_to_h1h1(sth, m1, m2, l112, Gam_SM):
    BRh2h1h1 = Gam_h2_to_h1h1(m1, m2, l112, sth) /  ( Gam_SM * sth**2  + Gam_h2_to_h1h1(m1, m2, l112, sth))
    return BRh2h1h1

# branching ratio RESCALING for h2 -> xx given Gamma_SM, sintheta, m1, m2, l112:
def RES_BR_h2_to_xx(sth, Gam_SM, m1, m2, l112):
    Gam_h2h1h1 = Gam_h2_to_h1h1(m1, m2, l112, sth)
    RES_h2xx = sth**2 * Gam_SM/ ( Gam_SM * sth**2  + Gam_h2h1h1)
    return RES_h2xx


# the file containing the branching ratios for the SM Higgs boson:
BR_file = "datafiles/higgsBR_YR4.txt"
# read the file:
print('reading in', BR_file)
HiggsBRs = read_higgsBR(BR_file)
# test: print the dictionary
# print_HiggsBR(HiggsBRs)
# first get the interpolated BRs and SM width 
BR_interpolators_SM = interpolate_HiggsBR(HiggsBRs)  # this returns the actual interpolators

# calculate the heavy Higgs boson branching ratios (no triple Higgs BR)
def calculate_heavy_BRs_only(interpolators_SM, mh1, mh2, l112, sintheta):
    heavyBRs = []
    if mh2 < 1000.: # 
        Gamma_SM = interpolators_SM[-1](mh2)
    else:
        Gamma_SM = interpolators_SM[-1](1000.)
        print("WARNING: mh2 > 1000.! (tree level)")
    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(sintheta, Gamma_SM, mh1, mh2, l112)
    # loop over the SM BRs and rescale with the factor:
    for hh in range(len(interpolators_SM)-1):
        if mh2 < 1000.:
            heavyBRs.append(interpolators_SM[hh](mh2) * rescale_fac)
        else:
            heavyBRs.append(interpolators_SM[hh](1000.) * rescale_fac)
    # add the h1h1 decay:
    BR_hh = BR_h2_to_h1h1(sintheta, mh1, mh2, l112, Gamma_SM)
    heavyBRs.append(BR_hh)
    # add the h1h1h1 decay (DON'T DO THIS HERE):
    BR_tripleHiggs = 0.
    heavyBRs.append(BR_tripleHiggs)

    # add the total heavy Higgs width:
    heavyBRs.append(width_h2(sintheta, mh1, mh2, l112, Gamma_SM))
    
    return heavyBRs

#(BR_interpolators_SM, key, v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta)
def convert_to_heavy_withtripleHiggs_OneLoop(interpolators_SM, name, v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta, costheta, calculate_triple=False):
    heavyBRs = []
    # get the masses and mixing angle

    #mh1, mh2, sintheta, costheta = mass_and_mixing(lam, v0, x0, a1, a2, b3, b4)
    lam, v0, x0, a1, a2, b2, b3, b4 = param_SARAH_to_MRM(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH)

    mh1, mh2 = OneLoop_masses_diag_scale_SARAH(91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    # get the corresponding SM width from the interpolator (this should be the last element):
    if mh2 < 1000.: # 
        Gamma_SM = interpolators_SM[-1](mh2)
    else:
        Gamma_SM = interpolators_SM[-1](1000.)
        print("WARNING: mh2 > 1000.! (tree level)")
    # get the lambda couplings
    #print 'Masses and mixing angles:'
    #print 'mh1, mh2=', mh1, mh2
    #print 'sintheta, costheta=', sintheta, costheta
    l112 = lambda112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    l1112 = lambda1112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    l111 = lambda111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    l122 = lambda122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    #print 'l111, l112, l122, l1112=', l111, l112, l122, l1112

    # get the total heavy Higgs width excluding the triple Higgs mode:
    # note that this already implies that decay width into triple Higgs << total width
    if mh1 > 80.:
        Gam1 = costheta**2 * interpolators_SM[-1](mh1) # WARNING: this is the width of the SM Higgs!
    else:
        print('WARNING, mh1 < 80.! (tree level)')
        Gam1 = interpolators_SM[-1](80.0)
    
    Gam2 = width_h2(sintheta, mh1, mh2, l112, Gamma_SM)

    #print 'Gam1, Gam2=', round_sig(Gam1,5), round_sig(Gam2,5)

    # FOR NOW EXCLUDE
    if mh2 > 3*mh1 and calculate_triple is True:
        width_hhh = Width_h2h1h1h1(l1112, l111, l112, l122, mh1, mh2, Gam1, Gam2)
    else:
        width_hhh = 0.

    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(sintheta, Gamma_SM, mh1, mh2, l112)
    # loop over the SM BRs and rescale with the factor:
    for hh in range(len(interpolators_SM)-1):
        if mh2 < 1000.:
            heavyBRs.append(interpolators_SM[hh](mh2) * rescale_fac)
        else:
            heavyBRs.append(interpolators_SM[hh](1000.) * rescale_fac)
    # add the h1h1 decay:
    BR_hh = BR_h2_to_h1h1(sintheta, mh1, mh2, l112, Gamma_SM)
    heavyBRs.append(BR_hh)

    # add the h1h1h1 decay (estimate!):
    BR_tripleHiggs = width_hhh/Gam2
    heavyBRs.append(BR_tripleHiggs)

    # add the total heavy Higgs width:
    heavyBRs.append(width_h2(sintheta, mh1, mh2, l112, Gamma_SM))
    
    return heavyBRs, round_sig(mh1,5), round_sig(mh2,5), round_sig(Gam1,5), round_sig(Gam2,5), round_sig(BR_hh,5), name

# print the heavy Higgs info:
BR_text_array_heavy_withtripleHiggs = [ '$b\\bar{b}$', '$\\tau \\tau$', '$\\mu \\mu$', '$c\\bar{c}$', '$s\\bar{s}$', '$t\\bar{t}$', '$gg$', '$\\gamma\\gamma$', '$Z \\gamma$', '$WW$', '$ZZ$', '$h_1 h1$', '$h_1 h_1 h_1$', '$\\Gamma$' ]
def print_heavy_Higgs_info(HeavyHiggsBRs, BR_text_array_heavy_triple, textinfo):
    print(textinfo)
    tbl = PrettyTable(["process", "BR"])
    for idx in range(len(HeavyHiggsBRs)):
      tbl.add_row([BR_text_array_heavy_triple[idx].replace('$', ''), round_sig(HeavyHiggsBRs[idx],5)])
    print(tbl)
    BRsum_heavy = 0.000
    for bb in HeavyHiggsBRs:
        if HeavyHiggsBRs.index(bb) != len(HeavyHiggsBRs)-1:
            BRsum_heavy = BRsum_heavy + round_sig(bb, 3)
    print('consistency check: sum(BRs)=', round_sig(BRsum_heavy,5))


##################################################################################
# SETUP THE INTERPOLATORS FOR THE CROSS SECTIONS AT VARIOUS ENERGIES/COLLIDERS   #
##################################################################################

# create interpolators for the XS and return a dictionary
def interpolate_HiggsXS(xsdict):
  # the kind of interpolation
  interpkind = 'linear'

  # define an array of interpolators
  interp_higgsxss = []

  # find out how many BRs+width we have:
  values_view = list(xsdict.values())
  value_iterator = iter(values_view)
  first_value = next(value_iterator)
  NXSs = len(first_value)
  
  # push back all the values of the masses, brs and width into arrays
  mass_array = []
  xs_array =[]

  # get the mass and the corresponding BR arrays
  for key in list(xsdict.keys()):
      mass_array.append(key)
      xs_array.append(xsdict[key][0])

  # now create the interpolators and put them in the array:
  interp_higgsxss = interp1d(mass_array, xs_array, kind=interpkind, bounds_error=False)

  return interp_higgsxss

# function to read in the XS into a dictionary in the format:
# mS or mH (GeV) | Cross Section (pb) |	+Theory | -Theory |	TH Gaussian | -+(PDF+alphaS)
# see https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWG#BSM_Higgs
def read_higgsXS_N3LO(xsfile):
    higgsxss = {}
    xsstream = open(xsfile, 'r')
    xsarray = []
    for line in xsstream:
        xsarray = [ float(line.split()[1]), float(line.split()[2]), float(line.split()[3]), float(line.split()[4]), float(line.split()[5])]
        higgsxss[float(line.split()[0])] = xsarray
    # sort by increasing value of HYmass
    sorted_x = sorted(list(higgsxss.items()), key=operator.itemgetter(0))
    sorted_higgsxss = collections.OrderedDict(sorted_x)
    return sorted_higgsxss

# function to read in the XS into a dictionary in the format:
# mH (GeV) |	Cross Section (pb) | +QCD Scale  | -QCD Scale | +-(PDF+alphaS) | +-PDF +| -alphaS | 1+delta EW
# see https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWG#BSM_Higgs
def read_higgsXS_N2LONNLL(xsfile):
    higgsxss = {}
    xsstream = open(xsfile, 'r')
    xsarray = []
    for line in xsstream:
        xsarray = [ float(line.split()[1]), float(line.split()[2]), float(line.split()[3]), float(line.split()[4]), float(line.split()[5]), float(line.split()[6]), float(line.split()[7])]
        higgsxss[float(line.split()[0])] = xsarray
    # sort by increasing value of HYmass
    sorted_x = sorted(list(higgsxss.items()), key=operator.itemgetter(0))
    sorted_higgsxss = collections.OrderedDict(sorted_x)
    return sorted_higgsxss

# function to read in the XS into a dictionary in the format:
# mS or mH (GeV) | Cross Section (pb)
def read_higgsXS_ihixs(xsfile):
    higgsxss = {}
    xsstream = open(xsfile, 'r')
    xsarray = []
    for line in xsstream:
        xsarray = [ float(line.split()[1])]
        higgsxss[float(line.split()[0])] = xsarray
    # sort by increasing value of HYmass
    sorted_x = sorted(list(higgsxss.items()), key=operator.itemgetter(0))
    sorted_higgsxss = collections.OrderedDict(sorted_x)
    return sorted_higgsxss

# the 13 TeV ggF cross sections at N^3LO
XS13_file = "datafiles/higgsXS_YR4_13TeV_N3LO.txt"
print('reading in', XS13_file)
HiggsXS_13_N3LO = read_higgsXS_N3LO(XS13_file)
# get the interpolated XS
XS_interpolator_SM_13TeV_N3LO = interpolate_HiggsXS(HiggsXS_13_N3LO)

# the 13 TeV ggF cross sections at NNLO+NNLL
XS13_file = "datafiles/higgsXS_YR4_13TeV_NNLONNLL.txt"
print('reading in', XS13_file)
HiggsXS_13_NNLONNLL = read_higgsXS_N3LO(XS13_file)
# get the interpolated XS
XS_interpolator_SM_13TeV_NNLONNLL = interpolate_HiggsXS(HiggsXS_13_NNLONNLL)

# the 14 TeV, 8 TeV and 7 TeV cross sections at NNLO+NNLL:
XS14_file = "datafiles/higgsXS_YR4_14TeV_N2LONNLL.txt"
HiggsXS_14_N2LONNLL = read_higgsXS_N2LONNLL(XS14_file)
XS_interpolator_SM_14TeV_NNLONNLL = interpolate_HiggsXS(HiggsXS_14_N2LONNLL)
XS8_file = "datafiles/higgsXS_YR4_8TeV_N2LONNLL.txt"
HiggsXS_8_N2LONNLL = read_higgsXS_N2LONNLL(XS8_file)
XS_interpolator_SM_8TeV_NNLONNLL = interpolate_HiggsXS(HiggsXS_8_N2LONNLL)
XS7_file = "datafiles/higgsXS_YR4_7TeV_N2LONNLL.txt"
HiggsXS_7_N2LONNLL = read_higgsXS_N2LONNLL(XS7_file)
XS_interpolator_SM_7TeV_NNLONNLL = interpolate_HiggsXS(HiggsXS_7_N2LONNLL)

# the 100 TeV cross section from ihixs:
XS100_file = "datafiles/higgsXS_ihixs_100TeV_N3LON3LL.txt"
HiggsXS_100_N3LON3LL = read_higgsXS_ihixs(XS100_file)
XS_interpolator_SM_100TeV_N3LON3LL = interpolate_HiggsXS(HiggsXS_100_N3LON3LL)

# 100 TeV VBF cross section at NLO from MadGraph:
XS100_VBF_file = "datafiles/higgsXS_VBF_100TeV_NLO.txt"
HiggsXS_100_VBF_NLO = read_higgsXS_ihixs(XS100_VBF_file)
XS_interpolator_SM_100TeV_VBF_NLO = interpolate_HiggsXS(HiggsXS_100_VBF_NLO)

# 27 TeV cross section from ihixs:
XS27_file = "datafiles/higgsXS_ihixs_27TeV_N3LON3LL.txt"
HiggsXS_27_N3LON3LL = read_higgsXS_ihixs(XS27_file)
XS_interpolator_SM_27TeV_N3LON3LL = interpolate_HiggsXS(HiggsXS_27_N3LON3LL)

# 27 TeV cross section to 100 TeV cross section RATIO from ihixs (GLUON FUSION):
XS27_RATIO_file = "datafiles/higgsXS_RATIO_ihixs_27to100TeV_N3LON3LL.txt"
RATIO_27_N3LON3LL = read_higgsXS_ihixs(XS27_RATIO_file)
RATIO_interpolator_SM_27TeV_N3LON3LL = interpolate_HiggsXS(RATIO_27_N3LON3LL)

# 27 TeV cross section to 100 TeV cross section RATIO from ihixs (GLUON FUSION):
XS27_QQ_RATIO_file = "datafiles/higgsXS_RATIO_QQ_27to100TeV_N3LON3LL.txt"
RATIO_QQ_27_N3LON3LL = read_higgsXS_ihixs(XS27_QQ_RATIO_file)
RATIO_QQ_interpolator_SM_27TeV_N3LON3LL = interpolate_HiggsXS(RATIO_QQ_27_N3LON3LL)

##############################################################
# HIGGSTOOLS SETUP: CALCULATE CURRENT COLLIDER CONSTRAINTS   #
##############################################################

pred = HP.Predictions() # create the model predictions

bounds = HB.Bounds('higgstools/hbdataset') # load HB dataset
signals = HS.Signals('higgstools/hsdataset') # load HS dataset

# add a SM-like Higgs boson with SM-like couplings

h = pred.addParticle(HP.NeutralScalar("h","even"))

# add second BSM Higgs boson which decays to two h bosons and is produced via gluon fusion

H = pred.addParticle(HP.NeutralScalar("H","even"))

# SM Higgs mass
Mh = 125.09
# set the SM Higgs mass
h.setMass(Mh)

# get the SM chi-squared for HiggsSignals
HP.effectiveCouplingInput(h, HP.scaledSMlikeEffCouplings(1.0))
ress_SM = signals(pred)
#print("HiggsSignals chi-sq. for SM=", ress_SM)

# minor correction to rescale all BRs to make sure that sum(BRs) = 1
def fix_heavy_BRs(heavyBRs):
    sumBRs = 0
    heavyBRs_fixed = []
    for i in range(0,12):
        sumBRs = sumBRs + heavyBRs[i]
    #print('sumBRs=',sumBRs)
    for j in range(len(heavyBRs)-1):
        heavyBRs_fixed.append(heavyBRs[j]/sumBRs)
    heavyBRs_fixed.append(heavyBRs[-1])
    return heavyBRs_fixed

# FUNCTION THAT RETURNS HiggsBounds True/False and chi-squared from HiggsSignals
# INPUT IS MH, sintheta and l112, the Scalar-Higgs-Higgs coupling
def check_singlet_point(MH, sintheta, costheta, l112, debug=False):
    # set the couplings of the SM-like Higgs boson to be rescaled according to costheta
    HP.effectiveCouplingInput(h, HP.scaledSMlikeEffCouplings(costheta))
    
    # set the mass of the heavy scalar and rescale the couplings according to sintheta (for production)
    # then set the BRs according to the calculation
    H.setMass(MH)
    HP.effectiveCouplingInput(H, HP.scaledSMlikeEffCouplings(sintheta))
    # calculate and print the heavy H branching ratios, given MH, lambda_112 and sintheta
    heavyBRs = calculate_heavy_BRs_only(BR_interpolators_SM, Mh, MH, l112, sintheta)
    heavyBRs = fix_heavy_BRs(heavyBRs)
    if debug is True:
        print_heavy_Higgs_info(heavyBRs, BR_text_array_heavy_withtripleHiggs, '\nHeavy Higgs BRs & width')
    # RESET BRs BEFORE SETTING THEM TO AVOID ISSUES WITH BR>1
    H.setBr('bb', 0.)
    H.setBr('tautau', 0.)
    H.setBr('mumu', 0.)
    H.setBr('cc', 0.)
    H.setBr('ss', 0.)
    H.setBr('tt', 0.)
    H.setBr('gg', 0.)
    H.setBr('gamgam', 0.)
    H.setBr('Zgam', 0.)
    H.setBr('WW', 0.)
    H.setBr('ZZ', 0.)
    # SET THE BRS
    H.setBr('bb', heavyBRs[0])
    H.setBr('tautau', heavyBRs[1])
    H.setBr('mumu', heavyBRs[2])
    H.setBr('cc', heavyBRs[3])
    H.setBr('ss', heavyBRs[4])
    H.setBr('tt', heavyBRs[5])
    H.setBr('gg', heavyBRs[6])
    H.setBr('gamgam', heavyBRs[7])
    H.setBr('Zgam', heavyBRs[8])
    H.setBr('WW', heavyBRs[9])
    H.setBr('ZZ', heavyBRs[10])
    H.setBr('h', 'h', heavyBRs[11])
    H.setTotalWidth(heavyBRs[13])


    # SOME TESTS HERE:
    # test whether the BRs have been set correctly
    #if debug is True:
    #    test_BR_array = [H.br('bb'), H.br('tautau'), H.br('mumu'), H.br('cc'), H.br('ss'), H.br('tt'), H.br('gg'), H.br('gamgam'), H.br('Zgam'), H.br('WW'), H.br('ZZ'), H.br('h', 'h'), 0., H.totalWidth()]
    #    print('gg > h cross section @ pp @ 13 TeV=', h.cxn('LHC13', "ggH"))
    #     print('gg > H cross section @ pp @ 13 TeV=', H.cxn('LHC13', "ggH"))
    #    # compare to independent calculations:
    #    xs13_nnlonnll = round_sig(sintheta**2 * XS_interpolator_SM_13TeV_NNLONNLL(MH),5)
    #    print('independent calculation of the cross section:')
    #    print('gg > H cross section @ pp @ 13 TeV (N^2LO+NNLL)=',  xs13_nnlonnll)
        

    # get and print the HiggsBounds results
    resb = bounds(pred)
    #print(resb)
    #print(resb.allowed)
    #print(resb.appliedLimits)
    #print([a for a in resb.appliedLimits if "H" in a.contributingParticles()])
    
    # get and print the HiggsSignal result
    ress = signals(pred)
    #print(signals(pred).appliedLimits)
    #print(ress)
    if ress - ress_SM < 3.84:
        ress_allowed = True
    else:
        ress_allowed = False

    # return HiggsBounds (True/False) for Allowed/Disallowed and the chi-squared from HiggsSignals
    return resb.allowed, ress_allowed

##########################################
# ELECTROWEAK PRECISION OBSERVABLES      #
##########################################

# ELECTROWEAK PRECISION OBSERVABLES:
# the GFITTER CURRENT central values and errors, see https://arxiv.org/pdf/1407.3792.pdf, page 11
# central values:
Delta_S_central = 0.06
Delta_T_central = 0.10
# errors:
errS = 0.09
errT = 0.07
# correlation (rho_12):
covST=0.91
# the GFITTER FUTURE central values and errors, see https://arxiv.org/pdf/1407.3792.pdf, page 13, Table 3
# central values:
Delta_S_central_F = 0.00
Delta_T_central_F = 0.00
# errors:
errS_F = math.sqrt(0.017**2 + 0.006**2) # experimental + theoretical errors in quadrature
errT_F = math.sqrt(0.022**2 + 0.005**2) 
# correlation (rho_12):
covST_F=0.91
# get the functions required for the EWPO chi-squared
# define the functions for calculation the changes S and T parameters:
# see hep-ph/9409380 appendix C
# x = mh**2 / mz**2
def B(x):
    resB = -1.
    if x >= 4:
        return np.sqrt(x * (x-4)) * np.log( 2/(np.sqrt(x) + np.sqrt(x-4)) )
    elif x < 4 and x >= 0:
        return np.sqrt(x * (4-x)) * np.arctan( np.sqrt(4/x -1 ) )
    elif x < 0:
        print("error, B(x) evaluated at x<0")
        exit()

def H_S(x):
    return (3./8.) * x - (1./12.) * x**2 + ( (3.-x)/4. + x**2/24. + 3./(4*(1-x)) ) * x * np.log(x) + (1 - x/3. + x**2/12.) * B(x) 


# c**2 = mw**2 / mz**2 
def H_T(x,c):
    return (3. * x / 4.) * ( np.log(x) / (1.-x) - np.log(x/c**2)/(1.-x/c**2) )

# the "observables"
def O_S(x):
    return (1./math.pi)* H_S(x)

def O_T(x, c, expansion_param):
    return expansion_param * H_T(x, c)

def Delta_S(m1, m2, sintheta, mz, mw):
    x1 = m1**2/mz**2
    x2 = m2**2/mz**2
    return sintheta**2 * (O_S(x2) - O_S(x1))

def Delta_T(m1, m2, sintheta, mz, mw):
    expansion_param = Gf * mz**2 / (2 * math.sqrt(2) * math.pi**2 * alpha )
    c = mw/mz
    x1 = m1**2/mz**2
    x2 = m2**2/mz**2
    return sintheta**2 * (O_T(x2, c, expansion_param) - O_T(x1,c, expansion_param))

# calculate the chisq for two correlated distributions, given the covariance off-diagonal elements cov12
def calc_chisquared_correlated(delta1, delta2, err1, err2, cov12, delta_central1, delta_central2):
    # calculate the inverse of the matrix V required for the chisq:
    Vinv = [[0 for xx in range(2)] for yy in range(2)]
    # now get the chisq:
    detfac = 1./(err1**2 * err2**2)/(1 - cov12**2)
    Vinv[0][0] = detfac * err2**2
    Vinv[0][1] = - detfac * cov12 * err1 * err2
    Vinv[1][0] = - detfac * cov12 * err1 * err2
    Vinv[1][1] = detfac * err1**2
    J0 = (delta_central1-delta1)
    J1 = (delta_central2-delta2)
    chisq_sum = J0 * Vinv[0][0] * J0 + J0 * Vinv[0][1] * J1 + J1 * Vinv[1][1] * J1 + J1 * Vinv[1][0] * J0
    return chisq_sum

# function to get the total chi_squared given m2, m1, sintheta, mz, mw, S, T central, S, T errors, covariance:
def get_chisq_EWPO(m1, m2, sintheta, mz, mw, Sc, Tc, errS, errT, covST):
    deltaS = Delta_S(m1, m2, sintheta, mz, mw)
    deltaT = Delta_T(m1, m2, sintheta, mz, mw)
    #print 'deltaS, deltaT=', deltaS, deltaT
    return calc_chisquared_correlated(deltaS, deltaT, errS, errT, covST, Sc, Tc)

# function to check whether the chi-sq from EWPO excludes or not  (at 2sigma)
def check_EWPO(m1, m2, sintheta, mz, mw, Sc, Tc, errS, errT, covST):
    chisq = get_chisq_EWPO(m1, m2, sintheta, mz, mw, Sc, Tc, errS, errT, covST)
    if chisq > 5.99:
        return False
    else:
        return True


################################################
# SETUP CLIC CONSTRAINTS                       #
################################################

# plots with kappa-m2 (kappa -> sintheta in the H -> VV case, kappa -> sintheta * BR(hh) in the H -> hh case)
# from 1807.04284
# CLIC H -> ZZ/WW (use BR=1):
CLIC_VV, CLIC_VV_tag = read_digit_plot_km2("Fig7_1807.04284", "CLIC-VV") # ok up to 1 TeV
# CLIC H -> hh 1.4 TeV (90% b-tagging):
CLIC14_HH, CLIC14_HH_tag = read_digit_plot_km2("Fig9orangedashed_1807.04284", "CLIC14-HH") # ok up to 1 TeV
# CLIC H -> hh 3 TeV (90% b-tagging):
CLIC3_HH, CLIC3_HH_tag = read_digit_plot_km2("Fig9bluedashed_1807.04284", "CLIC3-HH") # ok up to 1 TeV


################################################
# SETUP FCC-hh CONSTRAINTS                     #
################################################

# get the FCC 100 TeV limits (30/ab):
SMEARTAG = True
# 100 TeV H -> WW
FCC_WW_central, FCC_WW_1sigma, FCC_WW_2sigma, FCC_WW_tag = read_digit_plot("data_HWW100_L30000.0_signif2.0", "FCC-WW", smear=SMEARTAG)
# 100 TeV 30/ab DISCOVERY H -> WW
FCC_WW_DISC, FCC_WW_DISC_tag = read_digit_plot("data_dis_HWW100_L30000.0_signif5", "FCC-WW-5", smear=SMEARTAG)

# 100 TeV H -> ZZ
FCC_ZZ_central, FCC_ZZ_1sigma, FCC_ZZ_2sigma, FCC_ZZ_tag = read_digit_plot_multichannel(["data_HZZ100_4l_L30000.0_signif2.0", "data_HZZ100_2l2v_L30000.0_signif2.0", "data_HZZ100_2l2q_L30000.0_signif2.0"], "FCC-ZZ", smear=SMEARTAG)
# 100 TeV 30/ab DISCOVERY H -> ZZ
FCC_ZZ_DISC, FCC_ZZ_DISC_tag = read_digit_plot_multichannel_nosigmas(["data_dis_HZZ100_4l_L30000.0_signif5", "data_dis_HZZ100_2l2v_L30000.0_signif5", "data_dis_HZZ100_2l2q_L30000.0_signif5"], "FCC-ZZ-DISC", smear=SMEARTAG)

# 100 TeV H -> hh
FCC_HH_central, FCC_HH_1sigma, FCC_HH_2sigma, FCC_HH_tag = read_digit_plot("data_HH100_BBGG_L30000.0_signif2.0", "FCC-HH", smear=SMEARTAG)
# 100 TeV 30/ab DISCOVERY H -> hh
FCC_HH_DISC, FCC_HH_DISC_tag = read_digit_plot("data_dis_HH100_BBGG_L30000.0_signif5", "FCC-HH-5", smear=SMEARTAG)

# FCC DIFFERENT LUMINOSITIES:
Lumis = [100., 200., 500., 1000., 2000., 5000., 10000., 15000., 20000., 25000., 30000.]
NSIGMA = 2.0 # EXCLUSION
FCC_WW_lumi = {}
FCC_WW_lumi_tag = {}
FCC_HH_lumi = {}
FCC_HH_lumi_tag = {}
FCC_ZZ_lumi = {}
FCC_ZZ_lumi_tag = {}
for Lumi in Lumis:
    # 100 TeV H -> WW
    CurrentAnalysis = "HWW100"
    lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    FCC_WW_lumi[Lumi], FCC_WW_lumi_tag[Lumi] = read_digit_plot(lumifile, "FCC-WW-" + str(Lumi),  smear=SMEARTAG)
    # 100 TeV H -> hh
    CurrentAnalysis = "HH100_BBGG"
    lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    FCC_HH_lumi[Lumi], FCC_HH_lumi_tag[Lumi] = read_digit_plot(lumifile, "FCC-HH-" + str(Lumi),  smear=SMEARTAG)
    # 100 TeV H -> ZZ
    lumifilez1 =  "data_HZZ100_4l_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    lumifilez2 =  "data_HZZ100_2l2q_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    lumifilez3 =  "data_HZZ100_2l2v_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    FCC_ZZ_lumi[Lumi], FCC_ZZ_lumi_tag[Lumi] = read_digit_plot_multichannel_nosigmas([lumifilez1, lumifilez2, lumifilez3], "FCC-ZZ" + str(Lumi),  smear=SMEARTAG)


# get the FCC-hh (100 TeV) limits:
def check_fcc_limits(mh2, sinth, HeavyHiggsBRs):
    
    # get the 100 TeV cross sections:
    xs100_n3lon3ll = round_sig(sinth**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2),5)
    xs100_vbf_nlo = round_sig(sinth**2 * XS_interpolator_SM_100TeV_VBF_NLO(mh2),5)

    # get the sigma(h2) * BR for the processes to check:
    BR_ZZ = HeavyHiggsBRs[10]
    BR_WW = HeavyHiggsBRs[9]
    BR_hh = HeavyHiggsBRs[11]
    BR_hhh = HeavyHiggsBRs[12]
    
    xs100_WW = (xs100_n3lon3ll+xs100_vbf_nlo) * BR_WW
    xs100_ZZ = (xs100_n3lon3ll+xs100_vbf_nlo) * BR_ZZ
    xs100_HH = (xs100_n3lon3ll+xs100_vbf_nlo) * BR_hh
        
    # 100 TeV limits (30/ab):
    limit_FCC_WW_central, limit_FCC_WW_1sigma, limit_FCC_WW_2sigma, FCC_WW_process_tag = get_xsec_limit_future(mh2, FCC_WW_central, FCC_WW_1sigma, FCC_WW_2sigma, FCC_WW_tag)
    limit_FCC_ZZ_central, limit_FCC_ZZ_1sigma, limit_FCC_ZZ_2sigma, FCC_ZZ_process_tag = get_xsec_limit_future(mh2, FCC_ZZ_central, FCC_ZZ_1sigma, FCC_ZZ_2sigma, FCC_ZZ_tag)
    limit_FCC_HH_central, limit_FCC_HH_1sigma, limit_FCC_HH_2sigma, FCC_HH_process_tag = get_xsec_limit_future(mh2, FCC_HH_central, FCC_HH_1sigma, FCC_HH_2sigma, FCC_HH_tag)
    # 100 TeV limits (20/ab):
    Lumi = 20000
    limit_FCC_HH_lumi, limit_FCC_HH_lumi_tag = get_xsec_limit_current(mh2, FCC_HH_lumi[Lumi], FCC_HH_lumi_tag[Lumi])
    signif100_HH_lumi = xs100_HH * 2 / limit_FCC_HH_lumi
    limit_FCC_WW_lumi, limit_FCC_WW_lumi_tag = get_xsec_limit_current(mh2, FCC_WW_lumi[Lumi], FCC_WW_lumi_tag[Lumi])
    signif100_WW_lumi = xs100_WW * 2 / limit_FCC_WW_lumi
    limit_FCC_ZZ_lumi, limit_FCC_ZZ_lumi_tag = get_xsec_limit_current(mh2, FCC_ZZ_lumi[Lumi], FCC_ZZ_lumi_tag[Lumi])
    signif100_ZZ_lumi = xs100_ZZ * 2 / limit_FCC_ZZ_lumi
 
    # return the 20/ab 2sigma pass/fail:
    if signif100_HH_lumi > 2:
        fcc_hh = False
    else:
        fcc_hh = True

    if signif100_WW_lumi > 2:
        fcc_ww = False
    else:
        fcc_ww = True

    if signif100_ZZ_lumi > 2:
        fcc_zz = False
    else:
        fcc_zz = True


    # CHECK SIGNAL STRENGTH AS WELL:

    # FCC-hh and FCC-ee give similar constraints (see FCC Physics p. 53, table 4.4)    
    if sinth**2 > 0.01:
        couplstr_fut = False
    else:
        couplstr_fut = True

    return fcc_hh, fcc_ww, fcc_zz, couplstr_fut, xs100_WW, xs100_ZZ, xs100_HH


################################################
# W MASS CONSTRAINT                            #
################################################
# the W mass constraint (Tania Robens, Snowmass white paper)
datalist = [ ( np.loadtxt('datafiles/Tania_MW_SnowmassWhitepaper.dat'), 'total' ) ]
for data, label in datalist:
        x1w = data[:,0]
        y1w = data[:,1]
# interpolate the W mass constraint
interpkind = "cubic"
sinthetalim_wmass = interp1d(x1w, y1w, kind=interpkind, bounds_error=False)

def check_wmass_tania(mh2, sinth):
    sinthlim = sinthetalim_wmass(mh2)
    if sinth > sinthlim:
        return False
    else:
        return True

################################################
# PLOTTING FUNCTIONS                           #
################################################

##########################################################################
# PLOT: sintheta vs. mh2                                                 #
##########################################################################
def plot_sinthetamh2(df, dateofdata, outputdirectory):
    import pylab as plab
    print('---')
    print('plotting scatter plot for currently-viable points, including W mass constraint (Tania Robens)')
    # plot settings ########
    plot_type = 'ABSsintheta_M2_dod_' + dateofdata
    # plot:
    # plot settings
    ylab = '$\\left|\\sin \\theta\\right|$'
    xlab = '$m_2$ [GeV]'
    ymin = 0.0015
    ymax = 0.35
    xmin = 200.
    xmax = 1000
    ylog = True
    xlog = False

    # construct the axes for the plot
    gs = gridspec.GridSpec(16, 4)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)
                
  
    # the W mass constraint (Tania Robens, Snowmass white paper)
    datalist = [ ( np.loadtxt('datafiles/Tania_MW_SnowmassWhitepaper.dat'), 'total' ) ]
    for data, label in datalist:
        x1 = data[:,0]
        y1 = data[:,1]
    # interpolate the W mass constraint
    interpkind = "cubic"
    xi = np.linspace(100,1000, 900)
    f1 = interp1d(x1, y1, kind=interpkind, bounds_error=False)
    y1i = f1(xi)
    plt.plot(xi, y1i,color='black', lw=2, label='W mass (current)', ls='solid')

    # EWPO lines
    # fix m1 to = 125.09 GeV
    mh1 = 125.09
    m2_array = np.arange(100, 1200, 10) 
    sinth_array = np.arange(0, 0.4, 0.02)
    X,Y = np.meshgrid(m2_array, sinth_array)
    vget_chisq_EWPO = vectorize(get_chisq_EWPO)
    chisqi = vget_chisq_EWPO(mh1, X, Y, Mz, Mw, Delta_S_central, Delta_T_central, errS, errT, covST)# present
    chisqi_F  = vget_chisq_EWPO(mh1, X, Y, Mz, Mw, Delta_S_central_F, Delta_T_central_F, errS_F, errT_F, covST_F) # future
    cs = plt.contour(X,Y,chisqi, levels=[5.99], extend='both', colors='k', linestyles='dashed')
    #cs2 = plt.contour(X,Y,chisqi_F, levels=[5.99], extend='both', colors=['red'])
    plt.plot(np.nan, np.nan, color='black', lw=1, ls='--',ms=0,label='EWPO (current)')
    #plt.plot(np.nan, np.nan, color='red', lw=1, ms=0,label='EWPO (future)')

    
    # scatter plot of SFO-EWPT points:
    plt.scatter(df['mh2'], df['sinth']) 


    plt.ylim([ymin,ymax])
    plt.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    if xlog:
        ax.set_xscale('log')
    else:
        ax.set_xscale('linear')


    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="lower right", numpoints=1, frameon=True, prop={'size':6})
    #plt.rcParams.update({'font.size': 15})
    #plt.rcParams['figure.figsize'] = 12, 12
    
    # set the ticks, labels and limits
    xaxis = plt.gca().xaxis
    ax.xaxis.set_major_locator(MultipleLocator(200))
    ax.xaxis.set_minor_locator(MultipleLocator(50))
    yaxis = plt.gca().yaxis
    #yaxis.set_major_locator(MultipleLocator(0.05))
    #yaxis.set_minor_locator(MultipleLocator(0.02))
    ax.set_ylabel(ylab, fontsize=15)
    ax.set_xlabel(xlab, fontsize=15)

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    print('---')
    print('output in', outputdirectory + infile.replace('.dat','.pdf'))
    plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), dpi=400)
    plt.close(fig)

    reset_color()
    reset_cmap()


##########################################################################
# PLOT: FCC-HH XSEC LIMITS VS. CURRENTLY-VIABLE POINTS                   #
##########################################################################
def plot_fcclimits(df, dateofdata, outputdirectory):
    import pylab as plab
    print('---')
    print('plotting scatter plot for currently-viable points vs fcc limits')
    # plot settings ########
    plot_type = 'FCCxsecLimits_M2_dod_' + dateofdata
    # plot:
    # plot settings
    ylab = r'$\sigma(pp \rightarrow h_2 \rightarrow XX)$ [fb]'
    xlab = '$m_2$ [GeV]'
    ymin = 1E-5
    ymax = 1E3
    xmin = 200.
    xmax = 1000
    ylog = True
    xlog = False

    # construct the axes for the plot
    gs = gridspec.GridSpec(16, 4)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)
                  
    # scatter plot of SFO-EWPT points and their cross sections to each final state:
    # keep only points that satisfy current constraints
    dfvalid = df.drop(df[((df['hs'] == False) | (df['hb'] == False) | (df['wmass'] == False))].index)
    dfvalid = dfvalid.reset_index()
    
    plt.scatter(dfvalid['mh2'], dfvalid['xs100_WW'], color='blue',s=4) 
    plt.scatter(dfvalid['mh2'], dfvalid['xs100_ZZ'], color='red', s=4)
    plt.scatter(dfvalid['mh2'], dfvalid['xs100_HH'], color='black', s=4)

    # plot the limits:
    mh2_array = np.arange(100, 1200, 100) 
    WWlim = []
    ZZlim = []
    HHlim = []
    for mh2i in mh2_array:
        WWlimi, tag = get_xsec_limit_current(mh2i, FCC_WW_lumi[Lumi], FCC_WW_lumi_tag[Lumi])
        ZZlimi, tag = get_xsec_limit_current(mh2i, FCC_ZZ_lumi[Lumi], FCC_ZZ_lumi_tag[Lumi])
        HHlimi, tag = get_xsec_limit_current(mh2i, FCC_HH_lumi[Lumi], FCC_HH_lumi_tag[Lumi])
        WWlim.append(WWlimi)
        ZZlim.append(ZZlimi)
        HHlim.append(HHlimi)
    fww = interp1d(mh2_array, WWlim, kind='linear', bounds_error=False)
    fzz = interp1d(mh2_array, ZZlim, kind='linear', bounds_error=False)
    fhh = interp1d(mh2_array, HHlim, kind='linear', bounds_error=False)
   
    mh2a = np.arange(100, 1200, 0.1)
  
    WWlimI = fww(mh2a)
    ZZlimI = fzz(mh2a)
    HHlimI = fhh(mh2a)
    
    plt.plot(mh2a,WWlimI, color='blue', lw=2, label=r'$XX=W^+W^-$')
    plt.plot(mh2a,ZZlimI, color='red', lw=2, label=r'$XX=ZZ$')
    plt.plot(mh2a,HHlimI, color='black', lw=2, label=r'$XX=h_1 h_1$')


    plt.ylim([ymin,ymax])
    plt.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    if xlog:
        ax.set_xscale('log')
    else:
        ax.set_xscale('linear')


    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="lower right", numpoints=1, frameon=True, prop={'size':6})
    #plt.rcParams.update({'font.size': 15})
    #plt.rcParams['figure.figsize'] = 12, 12
    
    # set the ticks, labels and limits
    xaxis = plt.gca().xaxis
    ax.xaxis.set_major_locator(MultipleLocator(200))
    ax.xaxis.set_minor_locator(MultipleLocator(50))
    yaxis = plt.gca().yaxis
    #yaxis.set_major_locator(MultipleLocator(0.05))
    #yaxis.set_minor_locator(MultipleLocator(0.02))
    ax.set_ylabel(ylab, fontsize=15)
    ax.set_xlabel(xlab, fontsize=15)

    plt.title(r"Currently-viable points vs. FCC-hh limits (100 TeV@20 ab$^{-1}$)")


    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    print('---')
    print('output in', outputdirectory + infile.replace('.dat','.pdf'))
    plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), dpi=400)
    plt.close(fig)

    reset_color()
    reset_cmap()
