# script to run point generators for the singlet model multiple times on tiresias
import threading
from threading import Thread
import time
from time import sleep
import glob
from optparse import OptionParser
import subprocess
import numpy as np
import datetime
import sys
import logging
import os
from tqdm import tqdm
import string

# print intro
print("Launching PointGenerator threads on Tiresias")

YEAR = '2024'

# function to get template
def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

# write a filename
def writeFile(filename, text):
    with open(filename,'w') as f:
        f.write(text)


# template file for the point generator
TemplateTag = 'sintheta035'
TemplateFile = 'Point_generator_sintheta035_multirun.m'
Template = getTemplate(TemplateFile)
print('Template file is', TemplateFile)
print('with tag:', TemplateTag)

# template file for thefile writer
TemplateFile_thefile = 'point_manipulator_withx_multi.m'
Template_thefile = getTemplate(TemplateFile_thefile)

# location of the Mathematica executable (including the executable itself)
Mathematica_exec = '/Applications/Mathematica.app/Contents/MacOS/MathKernel'
# function to run a specific .m file
def Run_PointGenerator(thefile):
    #logging.info("Thread %s: starting", file_number)
    PointGenerator_Command = Mathematica_exec + ' -noprompt -run "<<' + thefile + '"'
    p = subprocess.Popen(PointGenerator_Command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Target_Directory)
    #for line in iter(p.stdout.readline, b''):
    #    print('\t\t', line, end=' ')
    out, err = p.communicate()
    #time.sleep(10)
    #logging.info("Thread %s: finishing", file_number)

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 4:
    print('usage: python run_pointgenerator_trs.py [mode=gen,run,merge] [target directory] [number of points per job] [max number of jobs]')
    exit()

Target_Directory = os.getcwd() + '/' + args[1]

# Check whether the specified path exists or not
isExist = os.path.exists(Target_Directory)

if not isExist:
  # Create a new directory because it does not exist 
  os.makedirs(Target_Directory)
  print("Working in new directory:", Target_Directory)

# check which mode to run
gen = False
run = False
merge = False
rerun = False
genthefiles = False
fixthefiles = False

if 'gen' == args[0]:
    gen = True

if 'run' == args[0]:
    run = True

if 'genrun' == args[0] or 'rungen' == args[0]:
    gen = True
    run = True

if 'merge' == args[0]:
    merge = True

if 'rerun' == args[0]:
    rerun = True

if 'genthefiles' == args[0]:
    genthefiles = True

if 'fixthefiles' == args[0]:
    fixthefiles = True
    
# get number of runs and number of points per run
nruns = int(args[3])
npoints = int(args[2])

if gen is True:
    print('Generating files to run in', Target_Directory)
    for counter in range(nruns):
        inputfile = Target_Directory + '/point_generator' + YEAR + '_' + TemplateTag + '_' + str(counter) + '.m'
        parmtextsubs = {
                'MAXPOINTS': str(npoints),
                'OUTPUT': Target_Directory.replace('/','//') + '//parameters' + YEAR + '_' + TemplateTag + '_' +str(counter) + '.m'
            }
        print('\t\twriting', inputfile)
        writeFile(inputfile, Template.substitute(parmtextsubs) )

if run is True:
    print('Running files generated')
    
    # launch the threads
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")
    #print(Max_Jobs)
    threads = list()
    logging.info("Main    : starting threads")
    for counter in tqdm(range(nruns)):
        time.sleep(1) # sleep for one second
        inputfile = Target_Directory + '/point_generator' + YEAR + '_' + TemplateTag + '_' + str(counter) + '.m'
        #logging.info("Main    : before creating thread")
        x = threading.Thread(target=Run_PointGenerator, args=(inputfile,))
        threads.append(x)
        x.start()
        #logging.info("Main    : wait for the thread to finish")
        # x.join()
    #logging.info("Main    : all done")
    for index, thread in enumerate(threads):
        #logging.info("Main    : before joining thread %d.", index)
        thread.join()
        logging.info("Main    : thread %d done", index)

    # finished processing PointGenerator Runs
    logging.info("Done with all point generator runs!")

if rerun is True:
    print('Re-running files that have failed')
    # launch the threads
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")
    #print(Max_Jobs)
    threads = list()
    logging.info("Main    : starting threads")
    for counter in tqdm(range(nruns)):
        filename_to_check = Target_Directory + '/parameters' + YEAR + '_' + TemplateTag + '_' +str(counter) + '.m'
        if os.path.exists(filename_to_check) is False:
            print(filename_to_check, 'does not exist, re-launching')
            inputfile = Target_Directory + '/point_generator' + YEAR + '_' + TemplateTag + '_' + str(counter) + '.m'
            #logging.info("Main    : before creating thread")
            x = threading.Thread(target=Run_PointGenerator, args=(inputfile,))
            threads.append(x)
            x.start()
            #logging.info("Main    : wait for the thread to finish")
            # x.join()
            #logging.info("Main    : all done")
    for index, thread in enumerate(threads):
        #logging.info("Main    : before joining thread %d.", index)
        thread.join()
        logging.info("Main    : thread %d done", index)

        # finished processing PointGenerator Runs
    logging.info("Done with all point generator runs!")

# merge the output files into a big .m file
if merge is True:
    print('Merging all the files')
    print('Writing merging mathematica script')
    mergingscript = Target_Directory + '/mergingscript.m'
    fmerge = open(mergingscript, "w")
    fmerge.write("listy = {};\n")
    for counter in tqdm(range(nruns)):
        filename_to_check = Target_Directory + '/parameters' + YEAR + '_' + TemplateTag + '_' +str(counter) + '.m'
        if os.path.exists(filename_to_check) is True:
            fmerge.write('list' + str(counter) + '=Import["' + Target_Directory.replace('/','//') + '//parameters' + YEAR + '_' + TemplateTag + '_' +str(counter) + '.m"]\n')
            fmerge.write('listy = Join[listy, list' + str(counter) + '];\n')
    mergedfile = Target_Directory.replace('/','//') + '//parameters' + YEAR + '_' + TemplateTag + '_merged.m'
    print('Merged file will be:', mergedfile)
    fmerge.write('Export["' + mergedfile + '", listy]\n')
    fmerge.write('Exit[];\n')
    print('Finished writing merging file')
    print('Launching merging script')
    Merging_Command = Mathematica_exec + ' -noprompt -run "<<' + mergingscript + '"'
    print(Merging_Command)
    p = subprocess.Popen(Merging_Command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Target_Directory)

# use the merged file and generate thefile*.txt
if genthefiles is True:
    print('Generating thefiles*.txt')
    mergedfile = Target_Directory.replace('/','//') + '//parameters' + YEAR + '_' + TemplateTag + '_merged.m'
    # get number of files:
    with open(mergedfile, 'r') as fp:
        for count, line in enumerate(fp):
            pass
    print('Total Lines', count + 1)
    nthefiles = int(count / 3 / 500)
    print('Total number of points in merged file:', int(count/3))
    print('Generating', nthefiles,'files')
    parmtextsubs = {
        'MERGEDFILE': mergedfile,
        'OUTPUTDIR': Target_Directory.replace('/','//'),
        'NTHEFILES': nthefiles
    }
    thefilegen = Target_Directory.replace('/','//') + '//point_manipulator_withx_multi_' + TemplateTag + '.m'
    print('\t\twriting', thefilegen)
    writeFile(thefilegen, Template_thefile.substitute(parmtextsubs) )
    print('Launching thefile generating script')
    GeneratingThefiles_Command = Mathematica_exec + ' -noprompt -run "<<' + thefilegen + '"'
    print(GeneratingThefiles_Command)
    process = subprocess.Popen(GeneratingThefiles_Command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Target_Directory)
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            print(output.strip())
    rc = process.poll()
    
if fixthefiles is True:
    print("Fixing thefiles*.txt")
    # the desired length for thefile lines
    thefile_line_length = 16
    minNum = 1
    mergedfile = Target_Directory.replace('/','//') + '//parameters' + YEAR + '_' + TemplateTag + '_merged.m'
    # get number of files:
    with open(mergedfile, 'r') as fp:
        for count, line in enumerate(fp):
            pass
    nthefiles = int(count / 3 / 500)
    maxNum = nthefiles
    #maxNum = 110
    print("From", minNum, 'to', maxNum)
    path = Target_Directory
    filetag = 'thefile_unfixed'
    filetagnew = 'thefile'
    fileext = '.txt'
    print("Reading in", filetag, "and writing out", filetagnew)
    for r in range(int(minNum), int(maxNum)+1):
        # check if file exists:
        thefile = path + '/' + filetag + str(r) + fileext
        thefilenew = path + '/' + filetagnew + str(r) + fileext
        if os.path.exists(thefile) is True:
            thefile_stream = open(thefile,'r')
            thefilenew_stream = open(thefilenew, 'w')
            count = 1
            print('writing out', thefilenew)
            for line in thefile_stream:
                # check that the length of the line is correct
                if len(line.split(',')) == thefile_line_length:
                    newline = line.rstrip().replace(' 10 ', ' ')
                    thefilenew_stream.write(newline + '\n')
                    count = count + 1
            thefile_stream.close()
            thefilenew_stream.close()
        else:
            print('Warning, file', thefile, 'does not exist')
            continue
    

