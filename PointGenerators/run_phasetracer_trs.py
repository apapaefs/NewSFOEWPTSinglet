# script to run phasetracer multiple times on tiresias
import threading
from threading import Thread
import time
from time import sleep
import glob
from optparse import OptionParser
import subprocess
import numpy as np
import datetime
import sys
import logging
import os
from tqdm import tqdm


# print intro
print("Launching PhaseTracer threads on Tiresias")

# location of the PhaseTracer executable (including the executable itself)
PhaseTracer_exec = '/Users/apapaefs/Projects/CosmoConstantSinglet/PhaseTracer/bin/RS'

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 1:
    print('usage: python run_phasetracer_trs.py [target directory with thefile*.txt] [max number of jobs] [rerun?]')
    exit()
    
# rerun?
rerun = False
if len(args) > 2 and args[2] == 'rerun':
        rerun = True
    
Input_Directory = os.getcwd() + '/' + args[0]

# set the max jobs manually.
# note that if Max_Jobs is higher than the available files, this will run over all files
UserDefined_Max_Jobs = False
Max_Jobs = 0
if len(args) > 1:
    Max_Jobs = int(args[1])
    UserDefined_Max_Jobs = True

def Run_PhaseTracer(file_number):
    #logging.info("Thread %s: starting", file_number)
    #PhaseTracer_Command = 'echo "TEST" >' + Input_Directory + '/testfile' + str(file_number)
    PhaseTracer_Command = PhaseTracer_exec + ' ' + Input_Directory + '/thefile' + str(file_number) + '.txt output' + str(file_number) + '.txt pointoutput' + str(file_number) + '.txt'
    p = subprocess.Popen(PhaseTracer_Command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Input_Directory)
    #for line in iter(p.stdout.readline, b''):
    #    print('\t\t', line, end=' ')
    out, err = p.communicate()
    #time.sleep(10)
    #logging.info("Thread %s: finishing", file_number)

def check_for_filepointoutput(directory, filenumber):
    thefile = directory+'/fullpointoutput' + str(filenumber) + '.txt'
    if os.path.exists(thefile):
        f = open(thefile, "r")
    else:
        return False
    if f.read(4) == 'DONE':
        return True
    else:
        return False
    f.close()
    
    
# count the number of files with "thefile*.txt" extension in Input_Directory:
filelist = glob.glob(Input_Directory + '/thefile?.txt') + glob.glob(Input_Directory + '/thefile??.txt') + glob.glob(Input_Directory + '/thefile???.txt')
if len(filelist) > 0:
    print('Number of files to process:', len(filelist))
    if UserDefined_Max_Jobs is True:
        if Max_Jobs < len(filelist):
            print('Processing up to', Max_Jobs, 'files')
        else:
            print('Processing all files')
    else:
        Max_Jobs = len(filelist)
        print('Processing all files')

else:
    print('Directory does not contain any files with the correct prefix (thefile*.txt), exiting...')
    exit()


# launch the threads
format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")
#print(Max_Jobs)
threads = list()
logging.info("Main    : starting threads")
for file_num in tqdm(range(1, Max_Jobs+1)):
    #logging.info("Main    : before creating thread")
    if rerun is False:
        x = threading.Thread(target=Run_PhaseTracer, args=(file_num,))
        threads.append(x)
        x.start()
    else:
        # check if fullpointoutputXXX.txt exists (and contains "DONE") before re-running
        filepointoutput_exists = check_for_filepointoutput(Input_Directory, file_num)
        if filepointoutput_exists is False:
            print("Launching missing job:", file_num)
            x = threading.Thread(target=Run_PhaseTracer, args=(file_num,))
            threads.append(x)
            x.start()
            x.join()
    #logging.info("Main    : wait for the thread to finish")
    # x.join()
#logging.info("Main    : all done")

    
#for index, thread in enumerate(threads):
#        #logging.info("Main    : before joining thread %d.", index)
#        
#        logging.info("Main    : thread %d done", index)

# finished processing PhaseTracer Runs
logging.info("Done with all PhaseTracer runs!")
