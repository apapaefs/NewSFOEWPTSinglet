# script to run phasetracer multiple times on tiresias
import threading
from threading import Thread
import time
from time import sleep
import glob
from optparse import OptionParser
import subprocess
import numpy as np
import datetime
import sys
import logging
import os
from tqdm import tqdm

# print intro
print("Checking PhaseTracer job completion in %")
