#! /usr/bin/env python
import sys
import time
import datetime
import os.path
from optparse import OptionParser
import subprocess

# remove weird lines

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 4:
    print 'usage: weirdfix_thefiles.py [input subfolder] [min #] [max #] [output subfolder]'
    exit()

print 'fixing weird spaces in thefileX.txt in', args[0], 'starting from', args[1], 'to', args[2]

filetag = 'thefile'
fileext = '.txt'

path = args[0]
minNum = args[1]
maxNum = args[2]
outpath = args[3]

# the desired length for thefile lines
thefile_line_length = 16

for r in range(int(minNum), int(maxNum)+1):
    # check if file exists:
    thefile = path + '/' + filetag + str(r) + fileext
    thefilenew = outpath + '/' + filetag + str(r) + fileext
    if os.path.exists(thefile) is True:
        thefile_stream = open(thefile,'r')
        thefilenew_stream = open(thefilenew, 'w')
        count = 1
        print 'writing out', thefilenew
        for line in thefile_stream:
            if len(line.split(',')) == thefile_line_length:
                newline = line.rstrip().replace(' 10 ', ' ')
                thefilenew_stream.write(newline + '\n')
                count = count + 1
        thefile_stream.close()
        thefilenew_stream.close()
    else:
        print 'Warning, file', thefile, 'does not exist'
        continue
    

