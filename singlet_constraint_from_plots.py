#! /usr/bin/env python
import math
from scipy.interpolate import interp1d
import numpy as np
import os.path
from os import path
import cmath

plot_location = './digitizationplots/' # SLASH AT END IMPORTANT!

# WARNING: HARD-CODED x range:
#x_full = [250.0, 300., 350., 400., 450., 500., 550., 600., 650., 700., 750., 800., 850., 900., 950., 1000.]

x_full = np.arange(200, 2050, 50.) 
print('x_full=',x_full)

# function to read-in a digitized plot and interpolate
# needs three files: central, upper 1 sigma, upper 2 sigma: extensions _central, _1sigma, _2sigma.txt
def read_digit_plot(file_tag, process_tag, smear=False):
    if smear is False:
        file_central = plot_location + file_tag + '_central.txt'
        file_1sigma = plot_location + file_tag + '_1sigma.txt'
        file_2sigma = plot_location + file_tag + '_2sigma.txt'
    elif smear is True:
        file_central = plot_location + file_tag + '_central.smear.txt'
        file_1sigma = plot_location + file_tag + '_1sigma.smear.txt'
        file_2sigma = plot_location + file_tag + '_2sigma.smear.txt'
    if path.exists(file_1sigma) and path.exists(file_2sigma):
        FuturePrediction = True
    else:
        FuturePrediction = False

    # open and read the files:
    stream_central = open(file_central, 'r')
    if FuturePrediction:
        stream_1sigma = open(file_1sigma, 'r')
        stream_2sigma = open(file_2sigma, 'r')
    x_central = []
    y_central = []
    for line in stream_central:
        x_central.append(float(line.split()[0]))
        y_central.append(float(line.split()[1]))
    if FuturePrediction:
        x_1sigma = []
        y_1sigma = []
        for line in stream_1sigma:
            x_1sigma.append(float(line.split()[0]))
            y_1sigma.append(float(line.split()[1]))
        x_2sigma = []
        y_2sigma = []
        for line in stream_2sigma:
            x_2sigma.append(float(line.split()[0]))
            y_2sigma.append(float(line.split()[1]))

    # interpolate in 1-D
    f_central = interp1d(x_central, y_central, kind='linear', bounds_error=False, fill_value=1.E99)
    if FuturePrediction:
        f_1sigma = interp1d(x_1sigma, y_1sigma, kind='linear', bounds_error=False, fill_value=1.E99)
        f_2sigma = interp1d(x_2sigma, y_2sigma, kind='linear', bounds_error=False, fill_value=1.E99)

    #m_min = x_central[0]
    #m_max = x_central[-1]
    # return the interpolators and the process tag
    if FuturePrediction:
        return f_central, f_1sigma, f_2sigma, process_tag
    else:
        return f_central, process_tag


# needs three files: central, upper 1 sigma, upper 2 sigma: extensions _central, _1sigma, _2sigma.txt
def read_digit_plot_km2(file_tag, process_tag):
    filename = plot_location + file_tag + '_km2.txt'

    # open and read the files:
    stream = open(filename, 'r')
    x = []
    y = []
    for line in stream:
        x.append(float(line.split()[0]))
        y.append(float(line.split()[1]))
    # interpolate in 1-D
    finterp = interp1d(x, y, kind='linear', bounds_error=False, fill_value="extrapolate")
    return finterp, process_tag

# function to read-in a digitized plot *WITHOUT INTERPOLATION*
# needs three files: central, upper 1 sigma, upper 2 sigma: extensions _central, _1sigma, _2sigma.txt
def read_digit_plot_nointerpolation(file_tag, process_tag, smearing=False):
    if smearing is False:
        file_central = plot_location + file_tag + '_central.txt'
        file_1sigma = plot_location + file_tag + '_1sigma.txt'
        file_2sigma = plot_location + file_tag + '_2sigma.txt'
    if smearing is True:
        file_central = plot_location + file_tag + '_central.smear.txt'
        file_1sigma = plot_location + file_tag + '_1sigma.smear.txt'
        file_2sigma = plot_location + file_tag + '_2sigma.smear.txt'
    if path.exists(file_1sigma) and path.exists(file_2sigma):
        FuturePrediction = True
    else:
        FuturePrediction = False

    # open and read the files:
    stream_central = open(file_central, 'r')
    if FuturePrediction:
        stream_1sigma = open(file_1sigma, 'r')
        stream_2sigma = open(file_2sigma, 'r')
    x_central = []
    y_central = []
    for line in stream_central:
        x_central.append(float(line.split()[0]))
        y_central.append(float(line.split()[1]))
    if FuturePrediction:
        x_1sigma = []
        y_1sigma = []
        for line in stream_1sigma:
            x_1sigma.append(float(line.split()[0]))
            y_1sigma.append(float(line.split()[1]))
        x_2sigma = []
        y_2sigma = []
        for line in stream_2sigma:
            x_2sigma.append(float(line.split()[0]))
            y_2sigma.append(float(line.split()[1]))

    return x_central, y_central, x_1sigma, y_1sigma, x_2sigma, y_2sigma

# function to read-in a digitized plot *WITHOUT INTERPOLATION*
# needs only file central: extension _central,
def read_digit_plot_nointerpolation_nosigmas(file_tag, process_tag, smearing=False):
    if smearing is False:
        file_central = plot_location + file_tag + '_central.txt'
    elif smearing is True:
        file_central = plot_location + file_tag + '_central.smear.txt'
    # open and read the files:
    stream_central = open(file_central, 'r')
    x_central = []
    y_central = []
    for line in stream_central:
        x_central.append(float(line.split()[0]))
        y_central.append(float(line.split()[1]))
    return x_central, y_central

# read in multiple files and pick the lowest upper bound as the upper bound at that mass point
# interpolate the result
# 1 and 2 sigma files MUST exist as well in this case
def read_digit_plot_multichannel(file_array, process_tag, smear=False):
    # dictionary that contains the interpolation results for each file:
    file_y_dict = {}

    # the following will hold the MINIMUM UPPER BOUND values of the y, y_1sigma and y_2sigma
    y_central_min = []
    y_1sigma_min = []
    y_2sigma_min = []
    # loop over the files and read in the plot
    for file_tag in file_array:
        x_central, y_central, x_1sigma, y_1sigma, x_2sigma, y_2sigma = read_digit_plot_nointerpolation(file_tag, process_tag, smearing=smear)
        # interpolate in 1-D
        f_central = interp1d(x_central, y_central, kind='linear', bounds_error=False, fill_value=1.E99)
        f_1sigma = interp1d(x_1sigma, y_1sigma, kind='linear', bounds_error=False, fill_value=1.E99)
        f_2sigma = interp1d(x_2sigma, y_2sigma, kind='linear', bounds_error=False, fill_value=1.E99)
        # get the current values for the x_full array:
        y_central_current = f_central(x_full)
        y_1sigma_current = f_1sigma(x_full)
        y_2sigma_current = f_2sigma(x_full)
        # put into the dictionary:
        file_y_dict[file_tag] = [y_central_current, y_1sigma_current, y_2sigma_current]

    # now loop through the x_full and find the minima:
    for xx in range(len(x_full)):
        yc_min = 1.E100
        y1_min = 1.E100
        y2_min = 1.E100
        for file_tag in file_array:
            yc = file_y_dict[file_tag][0][xx]
            y1 = file_y_dict[file_tag][1][xx]
            y2 = file_y_dict[file_tag][2][xx]
            if yc < yc_min and y1 < y1_min and y2 < y2_min:
                yc_min = yc
                y1_min = y1
                y2_min = y2
        y_central_min.append(yc_min)
        y_1sigma_min.append(y1_min)
        y_2sigma_min.append(y2_min)
        
    # now interpolate the results:
    f_central_min = interp1d(x_full, y_central_min, kind='linear', bounds_error=False, fill_value=1.E99)
    f_1sigma_min = interp1d(x_full, y_1sigma_min, kind='linear', bounds_error=False, fill_value=1.E99)
    f_2sigma_min = interp1d(x_full, y_2sigma_min, kind='linear', bounds_error=False, fill_value=1.E99)
        
    return f_central_min, f_1sigma_min, f_2sigma_min, process_tag

# read in multiple files and pick the lowest upper bound as the upper bound at that mass point
# interpolate the result
# 1 and 2 sigma files ARE NOT USED in this case
def read_digit_plot_multichannel_nosigmas(file_array, process_tag, smear=False):
    # dictionary that contains the interpolation results for each file:
    file_y_dict = {}
    # the following will hold the MINIMUM UPPER BOUND values of the y, y_1sigma and y_2sigma
    y_central_min = []
    # loop over the files and read in the plot
    for file_tag in file_array:
        x_central, y_central = read_digit_plot_nointerpolation_nosigmas(file_tag, process_tag, smearing=smear)
        # interpolate in 1-D
        f_central = interp1d(x_central, y_central, kind='linear', bounds_error=False, fill_value=1.E99)
        # get the current values for the x_full array:
        y_central_current = f_central(x_full)
        # put into the dictionary:
        file_y_dict[file_tag] = [y_central_current]

    # now loop through the x_full and find the minima:
    for xx in range(len(x_full)):
        yc_min = 1.E100
        for file_tag in file_array:
            yc = file_y_dict[file_tag][0][xx]
            if yc < yc_min:
                yc_min = yc
        y_central_min.append(yc_min)
        
    # now interpolate the results:
    f_central_min = interp1d(x_full, y_central_min, kind='linear', bounds_error=False, fill_value=1.E99)
        
    return f_central_min, process_tag


# get the upper limit given the interpolators and m2
def get_xsec_limit_future(m2, interp_central, interp_1sigma, interp_2sigma, process_tag):
    limit_central = interp_central(m2)
    limit_1sigma = interp_1sigma(m2)
    limit_2sigma = interp_2sigma(m2)
    return limit_central, limit_1sigma, limit_2sigma, process_tag

# get the upper limit given the interpolators and m2
def get_xsec_limit_current(m2, interp_central, process_tag):
    limit_central = interp_central(m2)
    return limit_central, process_tag

def check_sthetam2_limit(v0, stheta, m1, m2, l112, GammaSM, limtype, finterp, process_tag):
    FuncM2 = v0**2 * cmath.sqrt(1 - 4 * m1**2 / m2**2) / (8 * math.pi * GammaSM) # f(m2)
    R_lim = finterp(m2)
    stheta_sol = 0.
    # get the sintheta (sq) limit for the gauge boson decay type:
    if limtype == "VV":
        a = 1.
        b = - R_lim
        c = - (l112**2/v0**2) * FuncM2 * R_lim
        d = (b**2) - (4*a*c)
        # find two solutions, first one is -ve, so use second one. 
        sthetasq_sol1 = (-b-np.sqrt(d))/(2*a)
        sthetasq_sol2 = (-b+np.sqrt(d))/(2*a)
        sthetasq_sol = sthetasq_sol2.real
    elif limtype == "hh" or limtype == "HH" or limtype == "h1h1": # get the sintheta (sq) limit for the Higgs boson decay type:
        sthetasq_sol = l112**2 * FuncM2 * R_lim / ( l112**2 * FuncM2 - R_lim )
        sthetasq_sol = sthetasq_sol.real
    # check
    #print "limtype, stheta, stheta_sol=", limtype, stheta, math.sqrt(sthetasq_sol)
    if stheta**2 > sthetasq_sol:
        return False # excluded
    else:
        return True # not excluded

#######################
# TESTING BEGINS HERE #
#######################

test = False
if test is True:

    # test mass
    m2_test = 350.
    # read in the plots:
    zz_central, zz_1sigma, zz_2sigma, process_tag = read_digit_plot("Fig173_1902.00134", "ZZ")
    # use the interpolators to get the limit for the test mass:
    limit_m2_test_central, limit_m2_test_1sigma, limit_m2_test_2sigma, process_tag = get_xsec_limit_future(m2_test, zz_central, zz_1sigma, zz_2sigma, process_tag)

    print('limit on m2_test =', m2_test, ' is ',  limit_m2_test_central, limit_m2_test_1sigma, limit_m2_test_2sigma, ' pb') 

    # plots with kappa-m2 (kappa -> sintheta in the H -> VV case, kappa -> sintheta * BR(hh) in the H -> hh case)
    # from 1807.04284
    # CLIC H -> ZZ/WW (use BR=1):
    CLIC_VV, CLIC_VV_tag = read_digit_plot_km2("Fig7_1807.04284", "CLIC-VV")
    # CLIC H -> hh 3 TeV (90% b-tagging):
    CLIC3_HH, CLIC3_HH_tag = read_digit_plot_km2("Fig9bluedashed_1807.04284", "CLIC3-HH")
