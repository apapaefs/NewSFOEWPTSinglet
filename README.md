# Analysis of Strong First-Order Electro-Weak Phase Transition Points 

The purpose of this repository is to provide an updated analysis inspired by the arXiv:2010.00597.

## Usage

Execute ```singlet_constraint_calculator.py```. This will generate an output in the ```dataoutput``` directory. This will be in a compressed pickled pandas dataframe. 

To analyze the output, use the ```singlet_constraint_plotter.py``` script. This is meant to contain all the analysis of the results. 


## Pre-requisites (excluding PhaseTracer)

SciPy, NumPy, Matplotlib are all necessary.

You will need to compile HiggsTools. See the HiggsTools repository for details. 

It may also be necessary to install the latest version of PrettyTable: 

pip3 install -U git+https://github.com/jazzband/prettytable

## Point Generators and PhaseTracer for the real-singlet model

To get the point generators running:

1. Clone PhaseTracer: https://github.com/PhaseTracer/PhaseTracer 
2. Copy from PhaseTracerRS all the files into the corresponding PhaseTracer directories.
3. In PhaseTracer, do: ```mkdir build; cd build; cmake ..```
(you will need several libraries. If on a mac, brew tap brewsci/science and brew install alglib works but it will not find the include directory. Edit the generated ```build/CMakeCache.txt``` and add it: ```//Path to a file.
ALGLIB_INCLUDES:PATH=/opt/homebrew/include/alglib/```)
4. Compile: ```make -j12``` in the ```build``` directory
5. Now edit ```PointGenerators/run_pointgenerator_trs.py``` and find the ```Mathematica_exec``` variable, replace with the location of your Mathematica ```MathKernel```. 
6. Edit ```PointGenerators/run_phasetracer_trs.py``` and find the ```PhaseTracer_exec``` variable, replacing it with the location of the RS binary in your PhaseTracer directory. 
7. The steps to generate the scan through mathematica files are: 
```
python3 run_pointgenerator_trs.py gen pointsgenmulti_example 100 8
python3 run_pointgenerator_trs.py run pointsgenmulti_example 100 8
python3 run_pointgenerator_trs.py merge pointsgenmulti_example 100 8
python3 run_pointgenerator_trs.py genthefiles pointsgenmulti_example 100 8
```
The last command will generate thefileX.txt with 500 points each. A known issue is that it may not exit, so you may kill it in that case. 

```
python3 run_pointgenerator_trs.py fixthefiles pointsgenmulti_example 100 8
```
This will e.g. generate 100 points in 8 separate runs and then merge them. 

8. You can then run phase tracer on the outputs as:

```
python3 run_phasetracer_trs.py pointsgenmulti_example MAX_NUMBER_OF_JOBS
```
where ```MAX_NUMBER_OF_JOBS``` is e.g. the number of cores on your machine. 

Then run to get the files containing the different categories:
```
python3 postprocess_thefiles_rho_notrans.py pointsgenmulti_example MIN MAX
```

where ```MIN``` and ```MAX``` are the the minimum and maximum number of "thefiles" to process. 

