from singlet_constraint_functions import *

print('\n###########################')
print('Singlet SFO-EWPT Plotting')
print('###########################\n')

##########################################################################
# SETUP                                                                  #
##########################################################################

# and date of data:
dateofdata = '22122023'
# output directory 
outputdirectory = 'plots_' + str(todaysdate) + '_dod_' + (dateofdata) + '/'

# check if the plot directory exists and create if not
if not os.path.exists(outputdirectory):
    os.makedirs(outputdirectory)

##########################################################################
# READ THE DATA                                                          #
##########################################################################

print('Reading in data from dataoutput/data' + str(dateofdata)  + '.pkl.gz')
df = pd.read_pickle('dataoutput/data' + str(dateofdata)  + '.pkl.gz', compression='gzip')
print(df)


##########################################################################
# PLOTS START HERE                                                       #
##########################################################################

##########################################################################
# PLOT: sintheta vs. mh2                                                 #
##########################################################################
plot_sinthetamh2(df, dateofdata, outputdirectory)

##########################################################################
# PLOT: FCC-HH XSEC LIMITS VS. CURRENTLY-VIABLE POINTS                   #
##########################################################################
plot_fcclimits(df, dateofdata, outputdirectory)
