from singlet_constraint_functions import *

print('\n###########################')
print('Singlet SFO-EWPT Calculator')
print('###########################\n')


##########################################################################
# SETUP                                                                  #
##########################################################################

# DEBUG FLAG:
DEBUG = True

# SAVE THE DATA INTO A PANDAS DATAFRAME?
SAVEDATA = True
if SAVEDATA is True:
    data = []

# PRINT INFO ON POINTS THAT HAVE PASSED ALL CONSTRAINTS?
PRINTVALID = False
    
# the directory that contains the benchmark point files:
# "2022" denotes the last run
ProcessedBenchmarkDir2022 = "./PointGenerators"

# The dictionary that will hold the initial format of benchmark points:
RawBenchmarkPoints = {}

# The dictionary that will hold the format of benchmark points that will be processed:
BenchmarkPoints = {}

# The dictionary for the perturbative stability from the Snowmass 2022 study:
PerturbativityStability = {}

# which data points to include from the 2022 scan
INCLUDE_CENTR_2022 = False
INCLUDE_LIB_2022 = False
INCLUDE_CONS_2022 = True
INCLUDE_UCONS_2022 = False
INCLUDE_NOTRANS_2022 = False

# Determine the total maximum from each category of points:
MaxPoints = 10000


##########################################################################
# LOAD BENCHMARK POINTS                                                  # 
##########################################################################

print('Reading in processed files of benchmark points')

# Load the Spring 2022 Results:
if INCLUDE_CENTR_2022 is True:
        RawBenchmarkPoints_PROCESSED_CENTR = ReadProcessedBenchmarks(ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_2/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_3/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_4/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_5/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_6/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        RawBenchmarkPoints.update(RawBenchmarkPoints_PROCESSED_CENTR)
        
if INCLUDE_LIB_2022 is True:
        RawBenchmarkPoints_PROCESSED_LIB = ReadProcessedBenchmarks(ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8/pointoutput_liberal.txt', 'GWAPLib', MaxPoints)
        RawBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_2/pointoutput_liberal.txt', 'GWAPLib', MaxPoints)
        RawBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_3/pointoutput_liberal.txt', 'GWAPLib', MaxPoints)
        RawBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_4/pointoutput_liberal.txt', 'GWAPLib', MaxPoints)
        RawBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_5/pointoutput_liberal.txt', 'GWAPLib', MaxPoints)
        RawBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_6/pointoutput_liberal.txt', 'GWAPLib', MaxPoints)
        RawBenchmarkPoints.update(RawBenchmarkPoints_PROCESSED_LIB)

if INCLUDE_CONS_2022 is True:
        RawBenchmarkPoints_PROCESSED_CONS = ReadProcessedBenchmarks(ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8/pointoutput_conservative.txt', 'GWAPCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_2/pointoutput_conservative.txt', 'GWAPCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_3/pointoutput_conservative.txt', 'GWAPCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_4/pointoutput_conservative.txt', 'GWAPCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_5/pointoutput_conservative.txt', 'GWAPCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_6/pointoutput_conservative.txt', 'GWAPCons', MaxPoints)
        RawBenchmarkPoints.update(RawBenchmarkPoints_PROCESSED_CONS)

        PerturbativityStability_CONS = ReadProcessedBenchmarksPerturbativityStability(ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8/pertstabc.dat', 'GWAPCons', MaxPoints)
        PerturbativityStability_CONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_2/pertstabc.dat', 'GWAPCons', MaxPoints)
        PerturbativityStability_CONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_3/pertstabc.dat', 'GWAPCons', MaxPoints)
        PerturbativityStability_CONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_4/pertstabc.dat', 'GWAPCons', MaxPoints)
        PerturbativityStability_CONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_5/pertstabc.dat', 'GWAPCons', MaxPoints)
        PerturbativityStability_CONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_CONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_6/pertstabc.dat', 'GWAPCons', MaxPoints)
        PerturbativityStability.update(PerturbativityStability_CONS)

if INCLUDE_UCONS_2022 is True:
        RawBenchmarkPoints_PROCESSED_UCONS = ReadProcessedBenchmarks(ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8/pointoutput_fascist.txt', 'GWAPUCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_2/pointoutput_fascist.txt', 'GWAPUCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_3/pointoutput_fascist.txt', 'GWAPUCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_4/pointoutput_fascist.txt', 'GWAPUCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_5/pointoutput_fascist.txt', 'GWAPUCons', MaxPoints)
        RawBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_6/pointoutput_fascist.txt', 'GWAPUCons', MaxPoints)
        RawBenchmarkPoints.update(RawBenchmarkPoints_PROCESSED_UCONS)

        PerturbativityStability_UCONS = ReadProcessedBenchmarksPerturbativityStability(ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8/pertstabf.dat', 'GWAPUCons', MaxPoints)
        PerturbativityStability_UCONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_2/pertstabf.dat', 'GWAPUCons', MaxPoints)
        PerturbativityStability_UCONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_3/pertstabf.dat', 'GWAPUCons', MaxPoints)
        PerturbativityStability_UCONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_4/pertstabf.dat', 'GWAPUCons', MaxPoints)
        PerturbativityStability_UCONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_5/pertstabf.dat', 'GWAPUCons', MaxPoints)
        PerturbativityStability_UCONS = AddProcessedBenchmarksPerturbativityStability(PerturbativityStability_UCONS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_6/pertstabf.dat', 'GWAPUCons', MaxPoints)
        PerturbativityStability.update(PerturbativityStability_UCONS)

if INCLUDE_NOTRANS_2022 is True:
        RawBenchmarkPoints_PROCESSED_NOTRANS = ReadProcessedBenchmarks(ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8/pointoutput_notrans.txt', 'NoTrans', MaxPoints)
        RawBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_2/pointoutput_notrans.txt', 'NoTrans', MaxPoints)
        RawBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_3/pointoutput_notrans.txt', 'NoTrans', MaxPoints)
        RawBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_4/pointoutput_notrans.txt', 'NoTrans', MaxPoints)
        RawBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_5/pointoutput_notrans.txt', 'NoTrans', MaxPoints)
        RawBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(RawBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir2022 + '/pointsgenmulti_2022_sintheta035_20kx8_6/pointoutput_notrans.txt', 'NoTrans', MaxPoints)
        RawBenchmarkPoints.update(RawBenchmarkPoints_PROCESSED_NOTRANS)

print('Done reading benchmark files and perturbative stability files\n')
        
##########################################################################
# START LOOP OVER BENCHMARKS                                             # 
##########################################################################

print('Converting benchmark points to the right format')
for point in list(RawBenchmarkPoints.keys()):
    # DEBUG PRINT:
    if DEBUG:
        print('\n#####################################')
        print('point=', point)
        print('#####################################')

    # FORMAT: [ costh, sinth, mass2, gam2,  x0  , lamb, a_1, , a_2  , b_3   , b_4]
    costh, sinth, mass2, gam2, x0, lam, a1, a2, b3, b4 = ConvertBenchmarks(RawBenchmarkPoints[point])
    BenchmarkPoints[point] = costh, sinth, mass2, gam2, x0, lam, a1, a2, b3, b4

    # get the parameters from our benchmark in the SARAH format:
    v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sinth = GetSARAHParams(RawBenchmarkPoints[point])

    # get the tree-level triple scalar couplings:
    l111, l112, l122, l222, l1111, l1112, l1122, l1222, l2222 = calculate_lambdas(lam, a1, a2, x0, v0, costh, sinth, b3, b4)
    
    # find the perturbativity and stability results if they exist:
    # NOTE THAT ZERO IS PASSING PERTURBATIVITY/STABILITY AND ONE IS FAILING!!!!
    if point in PerturbativityStability.keys():
        Perturbativity = PerturbativityStability[point][0]
        Stability = PerturbativityStability[point][1]
    else:
        Perturbativity = -1
        Stability = -1

    # convert the SM BRs and width to Heavy Higgs BRs and width for the model parameters:
    # BR positions in the array: = [ '$b\\bar{b}$', '$\\tau \\tau$', '$\\mu \\mu$', '$c\\bar{c}$', '$s\\bar{s}$', '$t\\bar{t}$', '$gg$', '$\\gamma\\gamma$', '$Z \\gamma$', '$WW$', '$ZZ$', '$h_1 h_1$', '$h_1 h_1 h_1$', '$\\Gamma$' ]
    HeavyHiggsBRs, mh1, mh2, Gam1, Gam2, BR_hh, name = convert_to_heavy_withtripleHiggs_OneLoop(BR_interpolators_SM, point, v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sinth, costh, False)

    # get the light Higgs BRs as well -- rescaled by costheta:
    LightHiggsBRs = []
    for hh in range(len(BR_interpolators_SM)):
        LightHiggsBRs.append(BR_interpolators_SM[hh](mh1) * costh**2)

    ###################################
    # SOME MANUAL RESETS/CHECKS HERE  #
    ###################################
        
    # WARNING 
    # WARNING: SKIP POINT IF mh2 > 1000 GeV!
    # WARNING
    if mh2 > 1000.:
        continue

    # WARNING 
    # WARNING: SET THE HIGGS BOSON MASS HERE, IRRESPECTIVE OF mh1@1-loop
    # WARNING
    mh1 = 125.09
    
    #########################################
    # BEGIN CHECKS OF PARAMETER-SPACE POINT #
    #########################################

    if DEBUG:
        print_xsm_params(lam, v0, x0, a1, a2, b3, b4)
        print_SARAH_params_and_masses(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sinth, round_sig(mh1,5), round_sig(mh2,5))
        print_scalarcouplings(l111, l112, l122, l222, l1111, l1112, l1122, l1222, l2222)

    # HiggsTools Checks
    hb, hs = check_singlet_point(mh2, sinth, costh, l112, DEBUG)
    if DEBUG:
        print_htools(hb, hs)
    
    # get the EWPO constraints:
    # for the GFITTER CURRENT central values and errors, see https://arxiv.org/pdf/1407.3792.pdf, page 11
    # for the GFITTER FUTURE central values and errors, see https://arxiv.org/pdf/1407.3792.pdf, page 13, Table 3
    EWPO_cur = check_EWPO(mh1, mh2, sinth, Mz, Mw, Delta_S_central, Delta_T_central, errS, errT, covST) #current
    EWPO_fut = check_EWPO(mh1, mh2, sinth, Mz, Mw, Delta_S_central_F, Delta_T_central_F, errS_F, errT_F, covST_F) #future
    if DEBUG:
        print_ewpo(EWPO_cur, EWPO_fut)

    # get the W mass constraint:
    wmass = check_wmass_tania(mh2, sinth)
    if DEBUG:
        print_wmass(wmass)

    # get CLIC constraints (1807.04284):
    GammaSM = BR_interpolators_SM[-1](mh2) 
    CLIC14_HH_pass = check_sthetam2_limit(v0, abs(sinth), mh1, mh2, l112, GammaSM, "hh", CLIC14_HH, CLIC3_HH_tag)
    CLIC_VV_pass = check_sthetam2_limit(v0, abs(sinth), mh1, mh2, l112, GammaSM, "VV", CLIC_VV, CLIC_VV_tag) # this is also 3 TeV
    CLIC3_HH_pass = check_sthetam2_limit(v0, abs(sinth), mh1, mh2, l112, GammaSM, "hh", CLIC3_HH, CLIC3_HH_tag)
    if DEBUG:
        print_clic(CLIC14_HH_pass, CLIC_VV_pass, CLIC3_HH_pass)
        
    # get FCC-hh constraints (2010.00597):
    # also check that signal strength => sinth < 0.1: FCC-hh and FCC-ee give similar constraints (see FCC Physics p. 53, table 4.4)
    # and return the cross sections to WW, ZZ, HH 
    fcc_hh, fcc_ww, fcc_zz, couplstr_fut, xs100_WW, xs100_ZZ, xs100_HH = check_fcc_limits(mh2, sinth, HeavyHiggsBRs)
    if DEBUG:
        print_fcc(fcc_hh, fcc_ww, fcc_zz, couplstr_fut)
    
    
    # PRINT POINTS THAT PASS ALL CONSTRAINTS AT ALL COLLIDERS (if DEBUG is set to FALSE):
    if DEBUG is False and PRINTVALID is True:
        if hb and hs and EWPO_cur and EWPO_fut and CLIC14_HH_pass and CLIC_VV_pass and CLIC3_HH_pass and fcc_hh and fcc_ww and fcc_zz and couplstr_fut:
            print('\n###################################################')
            print('point=', point, 'remains after all tests')
            print('###################################################')
            print_xsm_params(lam, v0, x0, a1, a2, b3, b4)
            print_SARAH_params_and_masses(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sinth, round_sig(mh1,5), round_sig(mh2,5))
            print_scalarcouplings(l111, l112, l122, l222, l1111, l1112, l1122, l1222, l2222)
            print_heavy_Higgs_info(HeavyHiggsBRs, BR_text_array_heavy_withtripleHiggs, '\nHeavy Higgs BRs & width')

    if SAVEDATA:
        h2bb, h2tata, h2mumu, h2cc, h2ss, h2tt, h2gg, h2aa, h2zz, h2ww, h2zz, h2h1h1, h2h1h1h1, gam2 = HeavyHiggsBRs[0], HeavyHiggsBRs[1], HeavyHiggsBRs[2], HeavyHiggsBRs[3], HeavyHiggsBRs[4], HeavyHiggsBRs[5], HeavyHiggsBRs[6], HeavyHiggsBRs[7], HeavyHiggsBRs[8], HeavyHiggsBRs[9], HeavyHiggsBRs[10], HeavyHiggsBRs[11],HeavyHiggsBRs[12], HeavyHiggsBRs[13]
        data.append([point, costh, sinth, mass2, x0, lam, a1, a2, b3, b4, v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, mh1, mh2, l111, l112, l122, l222, l1111, l1112, l1122, l1222, l2222, h2bb, h2tata, h2mumu, h2cc, h2ss, h2tt, h2gg, h2aa, h2zz, h2ww, h2zz, h2h1h1, h2h1h1h1, gam2, xs100_WW, xs100_ZZ, xs100_HH, hb, hs, EWPO_cur, EWPO_fut, wmass, CLIC14_HH_pass, CLIC_VV_pass, CLIC3_HH_pass, fcc_hh, fcc_ww, fcc_zz, couplstr_fut])


##########################################################################
# END OF LOOP OVER BENCHMARKS                                            # 
##########################################################################

if SAVEDATA:
    df = pd.DataFrame(data, columns=('point', 'costh', 'sinth', 'mass2', 'x0', 'lam', 'a1', 'a2', 'b3', 'b4', 'v0_SARAH', 'x0_SARAH', 'mu2_SARAH', 'MS_SARAH', 'K1_SARAH', 'K2_SARAH', 'Kappa_SARAH', 'LambdaS_SARAH', 'Lambda_SARAH', 'mh1', 'mh2', 'l111', 'l112', 'l122', 'l222', 'l1111', 'l1112', 'l1122', 'l1222', 'l2222', 'h2bb', 'h2tata', 'h2mumu', 'h2cc', 'h2ss', 'h2tt', 'h2gg', 'h2aa', 'h2zz', 'h2ww', 'h2zz', 'h2h1h1', 'h2h1h1h1', 'gam2', 'xs100_WW', 'xs100_ZZ', 'xs100_HH', 'hb', 'hs', 'EWPO_cur', 'EWPO_fut', 'wmass', 'CLIC14_HH_pass', 'CLIC_VV_pass', 'CLIC3_HH_pass', 'fcc_hh', 'fcc_ww', 'fcc_zz', 'couplstr_fut'))
    #print(df)
    # check if the data output directory exists and create if not
    if not os.path.exists('dataoutput/'):
        os.makedirs('dataoutput/')
    df.to_pickle('dataoutput/data' + str(todaysdate)  + '.pkl.gz', compression='gzip')
